
 1. Comenzando con TexMaths Equations.

Para insertar su primera ecuación en un documento, simplemente escriba las instrucciones LaTex en el campo de texto de la ventana de ecuaciones TexMaths y de clic sobre el botón LaTeX.

Ejemplo: x(t) = \mu ^2 (t) debe producir la ecuación x(t)=µ²(t)


 2. Seleccionando el formato de la imagen.

TexMaths puede generar e insertar dos clases de imágenes en un documento LibreOffice.  Para seleccionar el formato de la imagen use el selector ubicado dentro del recuadro Formato de Imagen.

El formato de imagen por defecto es formato vectorial SVG.  Este permite que las ecuaciones LaTeX se les pueda cambiar el tamaño sin pérdida de calidad y no necesita instalarse ninguna fuente en especial en LibreOffice puesto que los símbolos matemáticos son dibujados usando curvas vectoriales.  Este es el formato preferido (y por defecto), pero puede haber algunos problemas cuando se exportan los documentos de LibreOffice a versiones viejas de MS Office como MS Office 2000.

El otro formato de imagen soportado es el PNG y es un formato de imagen de mapa de bits.  Así, usted tiene que seleccionar una resolución para usarlo.  La resolución por defecto se de 600 dpi, pero usted puede seleccionar otros valores distintos de los predefinidos.  No debe haber ningún problema de compatibilidad cuando se utiliza el formato PNG para la exportación a documentos de MS Office.

Note que cada formato está solamente disponible si el programa externo correspondiente (dvisvgm para SVG y dvipng para PNG) está instalado en su sistema con su ruta ajustada usando el cuadro de configuración de TexMaths. Por lo menos uno de esos dos programas se requiere para que TexMaths corra.


 3. Tamaño de la fuente

TexMaths puede manejar diferentes tamaños de letra para sus ecuaciones.  Para cambiar el tamaño de la fuente use el selector localizado dentro del recuadro del tamaño de fuente.

Puede seleccionar entre los tamaños predefinidos o escribir uno de su preferencia si este no está en la lista.


 4. Escribiendo ecuaciones

TexMaths puede generar tres tipos de ecuaciones LaTeX:
- ecuaciones tipo "display"
- ecuaciones que se muestran en línea
- ecucaciones LaTeX

Para seleccionar un tipo de ecuación, use el radio selector localizado dentro del recuadro Tipo de Ecuación.

Para generar ecuaciones tipo "display" o ecuaciones en línea tipo display, todo lo que tiene que hacer es seleccionar el tipo adecuado de ecuación y luego escribir el texto de la ecuación en el campo de texto de TexMaths en el diálogo de ecuación de TexMaths.

Ejemplo: Para generar la letra griega alpha, escriba \alpha

Las ecuaciones tipo "display" que van en línea se obtienen por la misma vía, pero la diferencia es que el estilo de la ecuación que se genera se integra mejor dentro del texto.  Por ejemplo, los índices de suma (\sum) y los exponentes se muestran cerca del signo suma y no abajo o arriba.
 
Ambas clases de ecuaciones son ideales para ecuaciones en una línea, el alineamiento vertical con el texto es correcto pero no es posible manejar ecuaciones multi-línea o ambientes más generales LaTeX.  Para esa clase de ecuaciones seleccione el tipo de ecuación LaTeX y escriba su ecuación dentro de las estructuras \begin{} y \end{}. Usted puede usar cualquier ambiente de instrucciones LaTeX.

Ejemplo: Escriba las siguientes líneas para mostrar una ecuación con tres líneas alineadas sobre el signo igual.
\begin{align*}
x &= 1 \\
y &= 2x+1\\
&= 3\\
\end{align*}


 5. Genere ecuaciones LaTeX

Una vez que usted ha escrito una ecuación, puede generarla dando clic sobre el botón denominado LaTeX del cuadro de diálogo TexMaths.  Esto genera una imagen de su ecuación en el formato, tipo y tamaño que usted seleccionó.


 6. Numerando las ecuaciones en Writer
 
Numerar las ecuaciones es tan simple como hacer clic sobre el icono TexMaths numerado (o dando una combinación de teclas que usted haya definido), escriba su ecuación y de clic sobre el botón LaTeX.  Una ecuación centrada y numerada se inserta automáticamente en su documento Writer.

Igualmente puede seleccionar el espaciado que desee y la etiqueta de la ecuación en la pestaña de opciones de Sistema de configuración de TexMaths.
 
 
 7. Edite ecuaciones LaTeX

Dentro de un documento LibreOffice, seleccione una imagen de una ecuación (ya creada con TexMaths) y de clic sobre el icono de ecuación TexMaths en la barra de herramientas (o use una combinación de teclas que haya definido).  Esta operación muestra el cuadro de diálogo de TexMaths de la ecuación que ha seleccionado.  Edite este texto y de clic sobre el botón LaTeX para generar la ecuación modificada.

De hecho usted puede cerrar un documento, abrirlo nuevamente y editar las ecuaciones que ha guardado.

 
 8. Texto a LaTeX en Writer
 
Escriba algún código LaTeX directamente en el documento Writer (p.e.  sin abrir el cuadro de diálogo TexMaths), seleccione el texto y de clic sobre el icono de ecuación de TexMaths (o use la combinación de teclas que haya definido).  El texto seleccionado es convertido a una imagen de ecuación LaTeX.  El formato aplicado a la imagen, el tamaño y el tiempo de ecuación son los seleccionados por defecto en el cuadro de diálogo de preferencias de TexMaths.
  

 9. Compatibilidad con ooolatex

Comenzando con la versión 0.34, TexMaths es compatible con ooolatex.  Esto significa que si usted tiene un documento que contiene ecuaciones ooolatex, puede editarlas seleccionándolas (una por una) y llamando el macro TexMaths para ecuaciones.  Puede entonces modificar el código LaTeX y regenerar la ecuación ya sea en formato SVG o PNG, como usted prefiera.


 10. Preferencias

TexMaths viene con algunos ajustes por defecto. Si estos ajustes no cumplen con sus necesidades puede cambiarlos usando el cuadro de diálogo Preferencias de TexMaths, el cual se llama dando clic en el botón Preferencias.  Este cuadro de diálogo le permite cambiar el formato de la imagen, el tamaño de la fuente, el tipo de ecuación, etc., para cada tipo de documento de LibreOffice.  Puede entonces guardar sus nuevos ajustes, restaurarlos a los que vienen por defecto o cargar ajustes que haya guardado con anterioridad.

 11. El preámbulo LaTeX

Dando clic sobre el botón preámbulo se muestra el cuadro de diálogo Preámbulo, el cual le permite ajustar su preámbulo LaTeX.  Por ejemplo, para generar ecuaciones coloreadas, descomente las siguientes dos líneas de preámbulo por defecto, cambie el valor del color RGB al que desee y grabe su preámbulo.

% \definecolor{myColor}{RGB}{0,0,255}
% \pagecolor{white}\color{myColor}

Ahora, todas las ecuaciones nuevas tendrán el esquema de color que haya definido en el preámbulo.


 12. Combinaciones de teclas

Para configurar las combinaciones de teclas lance TexMaths (con o sin numeración), de clic sobre el sistema de configuración TexMaths y defina sus combinaciones preferidas.  Para ecuaciones sin numeración usted puede tener una única combinación de teclas en todas las aplicaciones o diferentes para cada una. Puesto que las ecuaciones numeradas se pueden usar sólo en Writer, existe sólo una combinación de teclas para él.

Dentro del cuadro de diálogo de TexMaths usted puede usar las siguientes combinaciones de teclas:
- Esc para cerrar el cuadro de diálogo sin generar la ecuación
- Alt-L or Ctrl-L para generar una ecuación
- Alt-P or Ctrl-P para mostrar el diálogo de preferencias
- Alt-B or Ctrl-B para mostrar el diálogo del preámbulo

