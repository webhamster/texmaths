'
'    TexMathsPreamble
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

'	Macros used to create and manage the Preamble dialog


' Force variable declaration
Option Explicit


' Dialog declaration
Private oDlgPreamble as Variant

' Create and show dialog
Sub PreambleDialog()

	' Load the library TexMaths
	DialogLibraries.LoadLibrary( "TexMaths" )

	' Create the dialog object
	oDlgPreamble = createUnoDialog( DialogLibraries.GetByName("TexMaths").GetByName("TexMathsPreamble_GUI") )

	' Dialog strings for translation
	oDlgPreamble.setTitle( _("TexMaths Preamble") )
	oDlgPreamble.getControl("DefaultButton").Model.Label = _("Default")
	oDlgPreamble.getControl("DefaultButton").Model.HelpText = _("Load default preamble")
	oDlgPreamble.getControl("LoadButton").Model.Label = _("Load")
	oDlgPreamble.getControl("LoadButton").Model.HelpText = _("Load preamble saved into the current document")
	oDlgPreamble.getControl("ApplyButton").Model.Label = _("Apply")
	oDlgPreamble.getControl("ApplyButton").Model.HelpText = _("Apply current preamble")
	oDlgPreamble.getControl("SaveButton").Model.Label = _("Save")
	oDlgPreamble.getControl("SaveButton").Model.HelpText = _("Apply current preamble and save it into the current document")

	' Set the value
	oDlgPreamble.getControl("Preamble").setText(glb_sPreamble)
	
	' The button is activated if a preamble is stored in the current document
	If ReadPreamble() = 1 Then 
		oDlgPreamble.getControl("LoadButton").setEnable(TRUE)
	Else
		oDlgPreamble.getControl("LoadButton").setEnable(FALSE)
	End If

 	' Display the dialog
 	' This routine call does not return until the dialog is dismissed
    oDlgPreamble.Execute()                                                                          

End Sub


' Load the default preamble
Sub DefaultPreambleButton_Clicked()
	
	' Load the default preamble 
	DefaultPreamble()
	oDlgPreamble.getControl("Preamble").setText(glb_sPreamble)

End Sub


' Load the stored preamble if any
Sub LoadPreambleButton_Clicked()

	' As a fallback, load the default preamble 
	DefaultPreamble()

	' Try to read the stored preamble
	ReadPreamble()
	oDlgPreamble.getControl("Preamble").setText(glb_sPreamble)

End Sub


' Save the preamble to the config file
Sub SavePreambleButton_Clicked()

	' Set the preamble variable
	glb_sPreamble = oDlgPreamble.getControl("Preamble").getText()

	' Save the preamble to a UserDefinedProperties property
	Dim oDoc as Variant, oProperties as Variant
	
	oDoc = ThisComponent
	oProperties = oDoc.DocumentProperties.UserDefinedProperties
	
	' No LaTeX preamble already set => create it and set it
	If Not oProperties.PropertySetInfo.hasPropertyByName("TexMathsPreamble") Then
	
		' Encode the preamble string to avoid newline characters (workaround for an LO bug)
		oProperties.addProperty("TexMathsPreamble", com.sun.star.beans.PropertyAttribute.REMOVEABLE, EncodeNewline(glb_sPreamble))
	
	' Latex preamble already set => modify it
	Else
	
		' Encode the preamble string to avoid newline characters (workaround for an LO bug)
		oProperties.setPropertyValue("TexMathsPreamble", EncodeNewline(glb_sPreamble))
	
	End If

	oDlgPreamble.endExecute()
	
	DisplayStatus( _("Preamble applied and saved...") )
	
End Sub


' Apply button clicked
Sub ApplyPreambleButton_Clicked()
	
	' Set The preamble variable
	glb_sPreamble = oDlgPreamble.getControl("Preamble").getText()
	oDlgPreamble.endExecute()

	DisplayStatus( _("Preamble applied...") )
	
End Sub

