'
'    TexMathsEquations_Dlg
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

'	 Macros used to create and manage the TexMaths Equation dialog

' Force variable declaration
Option Explicit


' Dialog declaration
Private oDlgMain as Variant

' Create and display dialog
Sub TexMathsDialog( sEqSize as String, sEqType as String, sEqCode as String, sEqFormat as String, sEqDPI as String, sEqTransp as String, sEqName as String)

	' Load the library TexMaths
	DialogLibraries.LoadLibrary( "TexMaths" )
		
	' Create the dialog object
	oDlgMain = createUnoDialog( DialogLibraries.GetByName("TexMaths").GetByName("TexMathsEquations_GUI") )

	' Dialog strings for translation
	oDlgMain.setTitle( _("TexMaths Equations") )
	oDlgMain.getControl("EquationNameLabel").Model.Label = _("Name")
	oDlgMain.getControl("EquationNameText").Model.HelpText = _("Enter equation name here (can be left blank)")
	oDlgMain.getControl("EquationNameText").setText(sEqName)
	oDlgMain.getControl("ImageformatFrame").Model.Label = _("Image Format")
	oDlgMain.getControl("EquationtypeFrame").Model.Label = _("Equation Type")
	oDlgMain.getControl("FontsizeFrame").Model.Label = _("Font Size")
	oDlgMain.getControl("Format").Model.HelpText = _("Set image format")
	oDlgMain.getControl("GraphicDPI").Model.HelpText = _("Set image resolution")
	oDlgMain.getControl("Transparency").Model.Label = _("Transparency")
	oDlgMain.getControl("Transparency").Model.HelpText = _("Toggle image transparency")
	oDlgMain.getControl("Size").Model.HelpText = _("Set font size")
	oDlgMain.getControl("TypeDisplay").Model.HelpText = _("Inline Display equation")
	oDlgMain.getControl("TypeInline").Model.HelpText = _("Inline equation")
	oDlgMain.getControl("TypeLatex").Model.HelpText = _("Regular LaTeX code")
	oDlgMain.getControl("HelpButton").Model.Label = _("Help...")
	oDlgMain.getControl("HelpButton").Model.HelpText = _("Display help text")	
	oDlgMain.getControl("PreferencesButton").Model.Label = _("Preferences...")
	oDlgMain.getControl("PreferencesButton").Model.HelpText = _("Set equation preferences")	
	oDlgMain.getControl("PreambleButton").Model.Label = _("Preamble...")
	oDlgMain.getControl("PreambleButton").Model.HelpText = _("Set LaTeX preamble")	
	oDlgMain.getControl("LatexButton").Model.HelpText = _("Generate LaTeX equation")
	oDlgMain.getControl("IncWidthButton").Model.HelpText = _("Increase window width")	
	oDlgMain.getControl("DecWidthButton").Model.HelpText = _("Decrease window width")	
	oDlgMain.getControl("IncHeightButton").Model.HelpText = _("Increase window height")	
	oDlgMain.getControl("DecHeightButton").Model.HelpText = _("Decrease window height")	

	' Arrow icons for resize buttons
	oDlgMain.getControl("IncWidthButton").Model.ImageURL = glb_sPkgPath & "icons/right.png"	
	oDlgMain.getControl("DecWidthButton").Model.ImageURL = glb_sPkgPath & "icons/left.png"	
	oDlgMain.getControl("DecHeightButton").Model.ImageURL = glb_sPkgPath & "icons/up.png"	
	oDlgMain.getControl("IncHeightButton").Model.ImageURL = glb_sPkgPath & "icons/down.png"	

	' Define list values 
	oDlgMain.getControl("Format").addItems(Array("png","svg"),0)
	oDlgMain.getControl("GraphicDPI").addItems(Array("72","96","120","150","200","300","600","1200"),0)
	oDlgMain.getControl("Size").addItems(Array("6","7","8","9","10","10.5","11","12","13","14","15","16","18","20","22","24","26","28","32","36","40","44","48","54","60","66","72","80","88","96"),0)

	' Set the actual values
	oDlgMain.getControl("Size").setText(sEqSize)
	If sEqType = "display" Then oDlgMain.getControl("TypeDisplay").setState(1)
	If sEqType = "inline" Then oDlgMain.getControl("TypeInline").setState(1)
	If sEqType = "latex" Then oDlgMain.getControl("TypeLatex").setState(1)

	' Set text values
	oDlgMain.getControl("LatexCode").setText(sEqCode)
	oDlgMain.getControl("LogString").setText( glb_sStatus )
	oDlgMain.getControl("Format").setText(sEqFormat)
	oDlgMain.getControl("GraphicDPI").setText(sEqDPI)

	' Set transparency
	If sEqTransp = "TRUE" Then
		oDlgMain.getControl("Transparency").setState(1)
	Else
		oDlgMain.getControl("Transparency").setState(0)
	End If
	
	' Get the dvipng and dvisvgm paths saved in the registry
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)	

	' Transitional code
	' Window size unit has changed, so we reinit the sizes
	' and position at first run, otherwise the window will be too big
	If oSystemInfo.NewSizeUnit = "" Then
	
		oSystemInfo.PositionX = "127"
		oSystemInfo.PositionY = "85"
		oSystemInfo.Width = "306"
		oSystemInfo.Height = "250"
		oSystemInfo.NewSizeUnit = "1"
		oSystemInfo.commitChanges()
		
	End If
	' End of transitional code

	' Disable png and force svg if dvipng path is empty
	If oSystemInfo.DvipngPath = "" Then
		
		glb_sFormat = "svg"
		sEqFormat = glb_sFormat
		oDlgMain.getControl("Format").setText("svg")
		oDlgMain.getControl("Format").setEnable(FALSE)
	
	End If
			
	' Disable svg and force png if dvisvgm path is empty
	If oSystemInfo.DvisvgmPath = "" Then
		
		glb_sFormat = "png" 
		sEqFormat = glb_sFormat
		oDlgMain.getControl("Format").setText("png")
		oDlgMain.getControl("Format").setEnable(FALSE)
	
	End If

	' Disable dpi if svg image format
	If sEqFormat = "svg" Then 
		oDlgMain.getControl("GraphicDPI").setEnable(FALSE)
	Else 
		oDlgMain.getControl("GraphicDPI").setEnable(TRUE)
	End If
		
	' Set window position and size
	If oSystemInfo.PositionX <> "" Then oDlgMain.Model.PositionX = oSystemInfo.PositionX
	If oSystemInfo.PositionY <> "" Then oDlgMain.Model.PositionY = oSystemInfo.PositionY
	If oSystemInfo.Width <> "" Then ResizeWidth(oSystemInfo.Width)
	If oSystemInfo.Height <> "" Then ResizeHeight(oSystemInfo.Height)

    ' Execute dialog
    oDlgMain.Execute()

	' Save window position and size
	SavePosSize()

End Sub


' Set image format and enable or disable dpi according to the image format
Sub Format_Mod()

	If oDlgMain.getControl("Format").getText() = "svg" Then
		oDlgMain.getControl("GraphicDPI").setEnable(FALSE)
	Else 
		oDlgMain.getControl("GraphicDPI").setEnable(TRUE)
	End If

End Sub


' Set dpi value
Sub Dpi_Mod()

	glb_sGraphicDPI = oDlgMain.getControl("GraphicDPI").getText()

End Sub


' Set image transparency
Sub Trans_Mod()

	If oDlgMain.getControl("Transparency").getState() Then
		glb_sTransparency = "TRUE"
	Else
		glb_sTransparency = "FALSE"
	End If

End Sub


' Latex button clicked
Sub LatexButton_Clicked()

	Dim sEqCode as String, sEqType as String, iEqSize as Integer
	Dim sEqFormat as String, sEqDPI as String, sEqTransp as String

	' Get the general information used to compile the equation
	' Get the Latex code, the type of equation and the size of font
	sEqCode = oDlgMain.getControl("LatexCode").getText()
	sEqFormat = oDlgMain.getControl("Format").getText()
	sEqDPI = oDlgMain.getControl("GraphicDPI").getText()

	If oDlgMain.getControl("Transparency").getState() Then
		sEqTransp = "TRUE"
	Else
		sEqTransp = "FALSE"
	End If	

	iEqSize = oDlgMain.getControl("Size").getText()
	If oDlgMain.getControl("TypeDisplay").getState() Then sEqType = "display"
	If oDlgMain.getControl("TypeInline").getState() Then sEqType = "inline"
	If oDlgMain.getControl("TypeLatex").getState() Then sEqType = "latex"
		
	' Create the latex equation and insert it
	MakeEquation(iEqSize , sEqType , sEqCode, sEqFormat, sEqDPI, sEqTransp)

End Sub



' Help button clicked
Sub HelpButton_Clicked()

	glb_sStatus = ""
	oDlgMain.getControl("LogString").setText( glb_sStatus )
	HelpDialog()
	oDlgMain.getControl("LogString").setText( glb_sStatus )

End Sub


' Prefs button clicked
Sub PrefsButton_Clicked()

	glb_sStatus = ""
	oDlgMain.getControl("LogString").setText( glb_sStatus )
	PrefsDialog()
	oDlgMain.getControl("LogString").setText( glb_sStatus )

End Sub


' Preamble button clicked
Sub PreambleButton_Clicked()

	glb_sStatus = ""
	oDlgMain.getControl("LogString").setText( glb_sStatus )
	PreambleDialog()
	oDlgMain.getControl("LogString").setText( glb_sStatus )

End Sub


' Resize window width
Sub ResizeWidth(Width as Integer)
	
	Dim x as Integer, w as Integer
	Dim w1 as Integer, w2 as Integer, w3 as Integer, w4 as Integer, w5 as Integer
	Dim w6 as Integer, w7 as Integer, w8 as Integer, w9 as Integer, w10 as Integer
	Dim w11 as Integer, w12 as Integer, w13 as Integer, w14 as Integer, w15 as Integer
	Dim w16 as Integer, w17 as Integer

	w = oDlgMain.Model.Width

	w1 = w - oDlgMain.GetControl("LatexCode").Model.Width - oDlgMain.GetControl("LatexCode").Model.PositionX
	w2 = w - oDlgMain.GetControl("ImageformatFrame").Model.PositionX
	w3 = w - oDlgMain.GetControl("FontsizeFrame").Model.PositionX
	w4 = w - oDlgMain.GetControl("EquationtypeFrame").Model.PositionX
	w5 = w - oDlgMain.GetControl("Format").Model.PositionX
	w6 = w - oDlgMain.GetControl("GraphicDPI").Model.PositionX
	w7 = w - oDlgMain.GetControl("DpiLabel").Model.PositionX
	w8 = w - oDlgMain.GetControl("Size").Model.PositionX
	w9 = w - oDlgMain.GetControl("PtsLabel").Model.PositionX
	w10 = w - oDlgMain.GetControl("TypeDisplay").Model.PositionX
	w11 = w - oDlgMain.GetControl("TypeInline").Model.PositionX
	w12 = w - oDlgMain.GetControl("TypeLatex").Model.PositionX
	w13 = w - oDlgMain.GetControl("HelpButton").Model.PositionX
	w14 = w - oDlgMain.GetControl("PreferencesButton").Model.PositionX
	w15 = w - oDlgMain.GetControl("PreambleButton").Model.PositionX
	w16 = w - oDlgMain.GetControl("LatexButton").Model.PositionX
	w17 = w - oDlgMain.GetControl("Transparency").Model.PositionX

	' Set window width
	oDlgMain.Model.Width = Width

	' Compute widget margins
	' Compute and set new texfield width
	w = oDlgMain.Model.Width - oDlgMain.GetControl("LatexCode").Model.PositionX - w1
	oDlgMain.GetControl("LatexCode").Model.Width = w

	' Compute and set other widget positions
	w = oDlgMain.Model.Width
	x = w - w2 
	oDlgMain.GetControl("ImageformatFrame").Model.PositionX = x
	x = w - w3 
	oDlgMain.GetControl("FontsizeFrame").Model.PositionX = x
	x = w - w4 
	oDlgMain.GetControl("EquationtypeFrame").Model.PositionX = x
	x = w - w5 
	oDlgMain.GetControl("Format").Model.PositionX = x
	x = w - w6 
	oDlgMain.GetControl("GraphicDPI").Model.PositionX = x
	x = w - w7 
	oDlgMain.GetControl("DpiLabel").Model.PositionX = x
	x = w - w8 
	oDlgMain.GetControl("Size").Model.PositionX = x
	x = w - w9  
	oDlgMain.GetControl("PtsLabel").Model.PositionX = x
	x = w - w10 
	oDlgMain.GetControl("TypeDisplay").Model.PositionX = x
	x = w - w11 
	oDlgMain.GetControl("TypeInline").Model.PositionX = x
	x = w - w12 
	oDlgMain.GetControl("TypeLatex").Model.PositionX = x
	x = w - w13 
	oDlgMain.GetControl("HelpButton").Model.PositionX = x
	x = w - w14 
	oDlgMain.GetControl("PreferencesButton").Model.PositionX = x
	x = w - w15  
	oDlgMain.GetControl("PreambleButton").Model.PositionX = x
	x = w - w16 
	oDlgMain.GetControl("LatexButton").Model.PositionX = x
	x = w - w17 
	oDlgMain.GetControl("Transparency").Model.PositionX = x

End Sub


' Resize window height
Sub ResizeHeight(Height as Integer)

	Dim y as Integer, h as Integer
	Dim h1 as Integer, h2 as Integer, h3 as Integer, h4 as Integer, h5 as Integer
	Dim h6 as Integer

	' Compute widget margins
	h = oDlgMain.Model.Height

	h1 = h - oDlgMain.GetControl("LatexCode").Model.Height - oDlgMain.GetControl("LatexCode").Model.PositionY
	h2 = h - oDlgMain.GetControl("HelpButton").Model.PositionY
	h3 = h - oDlgMain.GetControl("PreferencesButton").Model.PositionY
	h4 = h - oDlgMain.GetControl("PreambleButton").Model.PositionY
	h5 = h - oDlgMain.GetControl("LatexButton").Model.PositionY
	h6 = h - oDlgMain.GetControl("LogString").Model.PositionY
	
	' Set window height
	oDlgMain.Model.Height = Height
	
	' Compute and set new textfield height
	h = oDlgMain.Model.Height - oDlgMain.GetControl("LatexCode").Model.PositionY - h1
	oDlgMain.GetControl("LatexCode").Model.Height = h

	' Compute and set other widget positions
	h = oDlgMain.Model.Height
	y = h - h2
	oDlgMain.GetControl("HelpButton").Model.PositionY = y
	y = h - h3
	oDlgMain.GetControl("PreferencesButton").Model.PositionY = y
	y = h - h4
	oDlgMain.GetControl("PreambleButton").Model.PositionY = y
	y = h - h5
	oDlgMain.GetControl("LatexButton").Model.PositionY = y
	y = h - h6
	oDlgMain.GetControl("LogString").Model.PositionY = y

End Sub


' Increase width button clicked
Sub IncreaseWidthButton_Clicked()

	Dim rw as Double
	Dim NewWidth as Integer

	' Compute new width
	NewWidth = oDlgMain.Model.Width + DLG_STEP_SIZE

	' Resize
	ResizeWidth(NewWidth)
		
	' Save window position and size
	SavePosSize()

End Sub


' Decrease width button clicked
Sub DecreaseWidthButton_Clicked()

	Dim rw as Double
	Dim NewWidth as Integer

	' Compute new width
	NewWidth = oDlgMain.Model.Width - DLG_STEP_SIZE

	' Keep the width above a minimum value
	If NewWidth < DLG_MIN_WIDTH Then
		Exit Sub
	End If

	' Resize
	ResizeWidth(NewWidth)
		
	' Save window position and size
	SavePosSize()

End Sub


' Increase height button clicked
Sub IncreaseHeightButton_Clicked()

	Dim rh as Double
	Dim NewHeight as Integer

	' Compute new width
	NewHeight = oDlgMain.Model.Height + DLG_STEP_SIZE

	' Resize
	ResizeHeight(NewHeight)

	' Save window position and size
	SavePosSize()

End Sub


' Decrease height button clicked
Sub DecreaseHeightButton_Clicked()

	Dim rh as Double
	Dim NewHeight as Integer

	' Compute new width
	NewHeight = oDlgMain.Model.Height - DLG_STEP_SIZE

	' Keep the height above a minimum value
	If NewHeight < DLG_MIN_HEIGHT Then
		Exit Sub
	End If

	' Resize
	ResizeHeight(NewHeight)

	' Save window position and size
	SavePosSize()

End Sub


' Save window position and size
Sub SavePosSize

	Dim oSystemInfo as Variant

	' Save window position and size
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)	

	' Workaround for a bug that sometimes occurs where model positions are not updated
	' We use the window positions to update the model positions
	' convX and convY are conversion factors between twips and map appfont
	Dim Pos As New com.sun.star.awt.Rectangle
	Dim convX as Double, convY as Double 
	Pos = oDlgMain.GetPosSize()

    If  oDlgMain.Model.Width = 0 Then
        oDlgMain.Model.Width = DLG_MIN_WIDTH
    End if

    if oDlgMain.Model.Height = 0 Then
         oDlgMain.Model.Height = DLG_MIN_HEIGHT
    End if

	convX = Pos.Width / oDlgMain.Model.Width
	convY = Pos.Height / oDlgMain.Model.Height

	' Sometimes convX or convY are equal to zero 
	If convX <> 0 and convY <> 0 Then 
		oDlgMain.Model.PositionX = Pos.X / convX
		oDlgMain.Model.PositionY = Pos.Y / convY
	End If
	' End of workaround

	oSystemInfo.PositionX = oDlgMain.Model.PositionX
	oSystemInfo.PositionY = oDlgMain.Model.PositionY
	oSystemInfo.Width = oDlgMain.Model.Width
	oSystemInfo.Height = oDlgMain.Model.Height
	
	oSystemInfo.commitChanges()

End Sub

