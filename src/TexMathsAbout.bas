'
'    TexMathsAbout
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

'	 Macros used to create and manage the About dialog 


' Force variable declaration
Option Explicit

Private oDlgAbout as Variant

' Main subroutine
Sub About()

	DefaultSysConfig()
	AboutDialog()

End Sub


' Create and show dialog
Sub AboutDialog()

	' Load the library TexMaths
	DialogLibraries.LoadLibrary("TexMaths")

	' Create the dialog object
	oDlgAbout = createUnoDialog( DialogLibraries.GetByName("TexMaths").GetByName("TexMathsAbout_GUI") )

	' Load package description, depending on the locale 
	Dim sDescPath as String, sFilePath as String, sMsg as String
	sDescPath = "help/description." & GetLocale()
	sFilePath = ConvertToURL( glb_sPkgPath & sDescPath )
	If Not FileExists( sFilePath ) Then	sDescPath =  "help/description.en"


	sMsg = ReadTextFileUtf8(sDescPath, glb_sPkgPath)
	sMsg = chr(10) & " TexMaths " & GetVersion() & chr(10) & sMsg
	oDlgAbout.getControl("TextField").setText(sMsg)

	' Dialog strings for translation
	oDlgAbout.setTitle( _("About TexMaths") )
	oDlgAbout.getControl("AboutButton").Model.Label = _("About")
	oDlgAbout.getControl("AboutButton").Model.HelpText = _("Display application informations")
	oDlgAbout.getControl("ChangelogButton").Model.HelpText = _("Display application changes")
	oDlgAbout.getControl("SystemlogButton").Model.HelpText = _("Display system informations")
	oDlgAbout.getControl("CloseButton").Model.Label = _("Close")
	oDlgAbout.getControl("CloseButton").Model.HelpText = _("Close window")

	' Change button states
	oDlgAbout.getControl("AboutButton").Model.State=1
	oDlgAbout.getControl("ChangelogButton").Model.State=0
	oDlgAbout.getControl("SystemlogButton").Model.State=0

	' Show the dialog window
    oDlgAbout.Execute()
    
End Sub


' Close dialog
Sub CloseAbout()

	oDlgAbout.endExecute()

End Sub


' Display about text
Sub AboutButton()

	Dim sDescPath as String, sFilePath as String, sMsg as String
	sDescPath = "help/description." & GetLocale()
	sFilePath = ConvertToURL( glb_sPkgPath & sDescPath )
	If Not FileExists( sFilePath ) Then	sDescPath =  "help/description.en"

	sMsg = ReadTextFileUtf8(sDescPath, glb_sPkgPath)
	sMsg = chr(10) & " TexMaths " & GetVersion() & chr(10) & sMsg
	oDlgAbout.getControl("TextField").setText( sMsg )

	' Change button states
	oDlgAbout.getControl("AboutButton").Model.State=1
	oDlgAbout.getControl("ChangelogButton").Model.State=0
	oDlgAbout.getControl("SystemlogButton").Model.State=0

End Sub


' Display ChangeLog
Sub ChangeLogButton()

	Dim sMsg as String
	sMsg = ReadTextFile("ChangeLog", glb_sPkgPath)
	oDlgAbout.getControl("TextField").setText( sMsg )

	' Disable current button state
	oDlgAbout.getControl("AboutButton").Model.State=0
	oDlgAbout.getControl("ChangelogButton").Model.State=1
	oDlgAbout.getControl("SystemlogButton").Model.State=0

End Sub


' Display system log
Sub SysLogButton()

	Dim sMsg as String
	If Not FileExists( glb_sUserPath & "System.log" ) Then

		sMsg = _("You have to generate a first equation before the system log can be displayed...")

	Else

		sMsg = ReadTextFile("System.log", glb_sUserPath)

	End If
	oDlgAbout.getControl("TextField").setText( sMsg )

	' Change button states
	oDlgAbout.getControl("AboutButton").Model.State=0
	oDlgAbout.getControl("ChangelogButton").Model.State=0
	oDlgAbout.getControl("SystemlogButton").Model.State=1

End Sub


