
'
'    TexMathsEquationsNumbered
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

'	 Macro used to create a numbered equation 

' Force variable declaration
Option Explicit

Sub Main
	
	' Get break before num and break after options
	Dim oSystemInfo as Variant
	Dim BreakBeforeNum as Boolean, BreakAfterNum as Boolean
	oSystemInfo = GetConfigAccess("/ooo.ext.texmaths.Registry/SystemInfo", TRUE)
	If oSystemInfo.BreakBeforeNum = "TRUE" Then	BreakBeforeNum = TRUE Else BreakBeforeNum = FALSE
	If oSystemInfo.BreakAfterNum = "TRUE" Then	BreakAfterNum = TRUE Else BreakAfterNum = FALSE

	' Get page width and margin sizes
	Dim oDoc as Variant, oPageStyles as Variant, oStyle as Variant, oViewCursor as Variant, oPageStyleName as Variant
	oDoc = ThisComponent
	
	' If we are in Writer preview mode, just silently exit
	If GetDocumentType(oDoc) = "swriter" Then
		If oDoc.getCurrentController.getFrame.LayoutManager.isElementVisible ( "private:resource/toolbar/previewobjectbar" ) Then Exit Sub
	End If 
	
	oViewCursor = oDoc.CurrentController.getViewCursor()
	oPageStyleName = oViewCursor.PageStyleName
	oPageStyles = oDoc.StyleFamilies.getByName("PageStyles")
	oStyle = oPageStyles.getByName(oPageStyleName)

	' Compute tab positions (middle of the page and border of the right margin)
	Dim pos1 as Integer, pos2 as Integer
	pos1 = (oStyle.Width - oStyle.LeftMargin - oStyle.RightMargin) / 2
	pos2 = oStyle.Width - oStyle.LeftMargin - oStyle.RightMargin

	' Get access to the document
	Dim oFrame as Variant
	Dim oDispatcher as Variant
	oFrame = ThisComponent.CurrentController.Frame
	oDispatcher = createUnoService("com.sun.star.frame.DispatchHelper")

	' Eventually insert an empty paragraph before the equation
	If BreakBeforeNum = TRUE Then oDispatcher.executeDispatch(oFrame, ".uno:InsertPara", "", 0, Array())

	' Insert two tab stops at positions pos1 and pos2
	' The first one is centered and the second one is right justified
	Dim args1(0) as new com.sun.star.beans.PropertyValue
	args1(0).Name = "Tabstops.TabStops"
	args1(0).Value = Array(Array(pos1,com.sun.star.style.TabAlign.CENTER,","," "),Array(pos2,com.sun.star.style.TabAlign.RIGHT,","," "))
	oDispatcher.executeDispatch(oFrame, ".uno:Tabstops", "", 0, args1())

	' Insert the equation caption text at the right
	Dim sCaption as String
	If len(oSystemInfo.EquationCaption) = 0 Then
		sCaption = "()"
	Else
		sCaption = "(" & oSystemInfo.EquationCaption & " )"
	End If
	args1(0).Name = "Text"
	args1(0).Value = CHR$(9) + CHR$(9) + sCaption
	oDispatcher.executeDispatch(oFrame, ".uno:InsertText", "", 0, args1())

	' Evetually insert an empty paragraph after the equation
	' And set the number of characters to go left
	Dim nleft as Integer
	If BreakAfterNum = TRUE Then
		oDispatcher.executeDispatch(oFrame, ".uno:InsertPara", "", 0, Array())
		nleft = 2
	Else
		nleft = 1
	End If

	' Go one or two characters left
	Dim args2(1) as new com.sun.star.beans.PropertyValue
	args2(0).Name = "Count"
	args2(0).Value = nleft
	args2(1).Name = "Select"
	args2(1).Value = false
	oDispatcher.executeDispatch(oFrame, ".uno:GoLeft", "", 0, args2())

	' Insert a numbering variable named Equation
	Dim args3(5) as new com.sun.star.beans.PropertyValue
	args3(0).Name = "Type"
	args3(0).Value = 23
	args3(1).Name = "SubType"

	' Set the numbering level
	If oSystemInfo.NumLevel = "0" Then
		args3(1).Value = 127	
	Else
		args3(1).Value = Val(oSystemInfo.NumLevel) - 1
	End If

	args3(2).Name = "Name"
	args3(2).Value = "Equation"
	args3(3).Name = "Content"
	args3(3).Value = ""
	args3(4).Name = "Format"
	args3(4).Value = 4
	args3(5).Name = "Separator"
	args3(5).Value = "."
	oDispatcher.executeDispatch(oFrame, ".uno:InsertField", "", 0, args3())

	' Go seven characters left
	args2(0).Name = "Count"
	args2(0).Value = len(sCaption)+1
	args2(1).Name = "Select"
	args2(1).Value = false
	oDispatcher.executeDispatch(oFrame, ".uno:GoLeft", "", 0, args2())

	' Set vertical alignment to middle
	dim args4(0) as new com.sun.star.beans.PropertyValue
	args4(0).Name = "VerticalParagraphAlignment"
	args4(0).Value = 3
	oDispatcher.executeDispatch(oFrame, ".uno:VerticalParagraphAlignment", "", 0, args4())

	' Launch the TexMaths Equation macro
	TexMaths.TexMathsEquations.main()

End Sub
