'
'    TexMathsPrefs
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

'    Macros related to main preferences

' Force variable declaration
Option Explicit


' Set user, temp and package paths
Sub DefaultSysConfig()

	' Service to access the user's paths information
	Dim aService as Variant, oFileAccess as Variant
	aService = CreateUnoService("com.sun.star.util.PathSubstitution")
	oFileAccess = createUnoService("com.sun.star.ucb.SimpleFileAccess")
	
	' User path and temp path
 	glb_sUserPath = aService.substituteVariables("$(user)", TRUE) & "/TexMaths/"
	glb_sTmpPath  = aService.substituteVariables("$(user)", TRUE) & "/TexMaths/tmp/"
          
	' Create the user directory if it doesn't exist
	If Not oFileAccess.exists(glb_sUserPath) Then MkDir(glb_sUserPath)
	If Not oFileAccess.exists(glb_sTmpPath)  Then MkDir(glb_sTmpPath)

	' Get the package path
	Const sPrefix = "vnd.sun.star.expand:"
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)
	glb_sPkgPath = Mid( oSystemInfo.PackageDir , len(sPrefix)+1)
	
	' Get the default context
	Dim oContext as Variant, oMacroExpander as Variant
	oContext = getProcessServiceManager().DefaultContext
	oMacroExpander = oContext.getValueByName("/singletons/com.sun.star.util.theMacroExpander")
	glb_sPkgPath = oMacroExpander.ExpandMacros(glb_sPkgPath)

End Sub


' Set default preferences
Sub DefaultPrefs()

	' Latex preferences
	glb_sWriterSize = "12"
	glb_sImpressSize = "28"
	glb_sDrawSize = "14"
	glb_sWriterEqType = "display"
	glb_sImpressEqType = "display"
	glb_sDrawEqType = "display"

	' Image preferences
	glb_sFormat = "svg"

	' Force default to png if the dvisvgm path is empty
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)	
	If oSystemInfo.DvisvgmPath = "" Then
		glb_sFormat = "png"
	End If

	glb_sGraphicDPI = "600"
	glb_sTransparency = "FALSE"
	glb_sStatus = _("Default preferences loaded...")

End Sub


' Return a string with the current preferences
' to be written to the preferences file
Function Preferences() as String

	Preferences() = _
	"GraphicDPI=" & glb_sGraphicDPI & chr(10) &_
	"FileFormat=" & glb_sFormat & chr(10) &_
	"Transparency=" & glb_sTransparency & chr(10) &_
	"WriterCharSize=" & glb_sWriterSize & chr(10) &_
	"WriterEquationType=" & glb_sWriterEqType & chr(10) &_
	"ImpressCharSize=" & glb_sImpressSize & chr(10) &_
	"ImpressEquationType=" & glb_sImpressEqType & chr(10) &_
	"DrawCharSize=" & glb_sDrawSize & chr(10) &_
	"DrawEquationType=" & glb_sDrawEqType & chr(10)

End Function


' Read the user's preferences file
' Call the routine for data parsing
Sub ReadPrefsFile()

	Dim iNumber as Integer
	Dim sLine,sPrefs as String
	sPrefs = ""
	iNumber = Freefile
	Open glb_sUserPath & "TexMaths.cfg" For Input As iNumber
	While Not EOF(iNumber)

		Line Input #iNumber, sLine
		If sLine <> "" Then sPrefs = sPrefs & chr(10) & sLine 

	Wend
	Close #iNumber
	ReadPreferencesFrom( sPrefs )

	glb_sStatus = _("User's preferences loaded...")

End Sub



' This fonction parses the preferences from string sPrefs
Sub ReadPreferencesFrom( sPrefs as String )

	Dim i as Integer, ii as Integer
	Dim sLine() as String, sArguments() as String

	If StringNotContains( sPrefs , chr(10)) Then Exit Sub
	sLine() = Split( sPrefs , chr(10))
	ii = 0
	
	For i = 0 to UBound(sLine)

		If Not StringNotContains(sLine(i),"=") Then 

			sArguments() = Split(sLine(i),"=")

			Select Case sArguments(0)

				Case "ScriptPath"
					Dim sScriptPath as String
					sScriptPath = CheckPath(ConvertToURL(sArguments(1)))
				Case "GraphicDPI"
					glb_sGraphicDPI = sArguments(1)
				Case "FileFormat"
					glb_sFormat = sArguments(1)
				Case "Transparency"
					glb_sTransparency = sArguments(1)
				Case "WriterCharSize"
					glb_sWriterSize = sArguments(1)
				Case "WriterEquationType"
					glb_sWriterEqType = sArguments(1)
				Case "ImpressCharSize"
					glb_sImpressSize = sArguments(1)
				Case "ImpressEquationType"
					glb_sImpressEqType = sArguments(1)
				Case "DrawCharSize"
					glb_sDrawSize = sArguments(1)
				Case "DrawEquationType"
					glb_sDrawEqType = sArguments(1)

			End Select

		End If

		ii = ii + 1 + Len(sLine(i))
		If Len(sPrefs) < ii  Then Exit For

	Next

End Sub


' Set default preamble
Sub DefaultPreamble()

	' Set the preamble to default value
	glb_sPreamble = "\usepackage{amsmath}" & chr(10) &_
			"\usepackage{amssymb}" & chr(10) &_
		    "\usepackage[usenames]{color}" & chr(10) &_
		    chr(10) &_
            "% Uncomment this line for sans-serif font" & chr(10) &_
            "%\everymath{\mathsf{\xdef\mysf{\mathgroup\the\mathgroup\relax}}\mysf}" & chr(10) &_
		    chr(10) &_
	    	"% Uncomment these lines for colored equations" & chr(10) &_
	    	"% \definecolor{fgcolor}{RGB}{0,0,255}" & chr(10) &_
	    	"% \definecolor{bgcolor}{RGB}{255,255,255}" & chr(10) &_
	    	"% \pagecolor{bgcolor}\color{fgcolor}" &_
	    	chr(10)

End Sub


' Read preamble - Return 1 if a preamble was found, else return 0
Function ReadPreamble() as Integer
	
	' Read the preamble from a UserDefinedProperties property
	Dim oDoc as Variant, oProperties as Variant
	
	oDoc = ThisComponent
	oProperties = oDoc.DocumentProperties.UserDefinedProperties
	
	' No stored LaTeX preamble
	If Not oProperties.PropertySetInfo.hasPropertyByName("TexMathsPreamble") Then
	
		ReadPreamble() = 0
		Exit Function
	
	' Read stored LaTeX preamble
	Else
	
		' Decode the stored premable string to restore the newline characters (workaround for an LO bug)
		glb_sPreamble = DecodeNewline( oProperties.getPropertyValue("TexMathsPreamble") )
		ReadPreamble() = 1
	
	End If

End Function

