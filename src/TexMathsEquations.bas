'
'    TexMathsEquations
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

' 	 Macros used to create the LaTeX equation

' Force variable declaration
Option Explicit


' Global constants
Global Const DLG_MIN_HEIGHT = 250
Global Const DLG_MIN_WIDTH = 200
Global Const DLG_STEP_SIZE = 5
Global Const ANCHOR_TO_PARA = 0
Global Const ANCHOR_TO_CHAR = 1
Global Const ANCHOR_TO_PAGE = 2
Global Const ANCHOR_AT_CHAR = 4


' Global preferences variables
Global glb_sUserPath as String, glb_sTmpPath as String, glb_sPkgPath as String
Global glb_sPreamble as String, glb_sFormat as String
Global glb_sWriterSize as String, glb_sWriterEqType as String
Global glb_sImpressSize as String, glb_sImpressEqType as String
Global glb_sDrawSize as String, glb_sDrawEqType as String
Global glb_sGraphicDPI as String, glb_sTransparency as String
Global glb_sStatus as String
Global glb_WordVertAlign as Boolean

' Global translation variables
Global glb_PoFileRead as Integer
Global glb_MsgId(0) as Variant
Global glb_MsgStr(0) as Variant

' Variables related to the edit equation mode
Public oShapePosition as Variant
Public EditEquation as Boolean, Dialog as Boolean



' Main subroutine
Sub main
  
	' The Draw component is required
	If ComponentInstalled( "Draw" ) = FALSE Then

  		msgbox( _("Please install the Draw component to run TexMaths!") )
  		Exit Sub

	End If	 
 
	' Initialize object to null to allow testing if it was created
	oDlgMain = null
	
	' Set TexMaths configuration
	SetConfig()
	
	' Check program paths
	CheckProgramPaths()
	
	' Exit if configuration was not saved
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess("/ooo.ext.texmaths.Registry/SystemInfo", TRUE)
	If oSystemInfo.ConfigSaved = "" Then Exit Sub
	
	' Get Word vertical alignment compatibility option
	If oSystemInfo.WordVertAlign = "TRUE" Then	glb_WordVertAlign = TRUE Else glb_WordVertAlign = FALSE

	' Get the current document and controller
 	Dim oDoc as Variant, oDocCtrl as Variant
 	oDoc = ThisComponent
	oDocCtrl = oDoc.getCurrentController()

	' Test if we are in Writer, Impress or Draw and set the default user preferences
	Dim sEqSize as String, sEqType as String, sEqLatexCode as String, sEqFormat as String, sEqDPI as String, sEqTransp as String, sEqName as String

	' Default image format, graphic DPI and transparency
	sEqFormat = glb_sFormat
	sEqDPI = glb_sGraphicDPI
	sEqTransp = glb_sTransparency

	If GetDocumentType(oDoc) = "swriter" Then

		' If we are in Writer preview mode, just silently exit
		If oDoc.getCurrentController.getFrame.LayoutManager.isElementVisible ( "private:resource/toolbar/previewobjectbar" ) Then Exit Sub

		' We are in a Writer doc, set default preferences
		sEqSize = glb_sWriterSize
		sEqType = glb_sWriterEqType
		sEqLatexCode = ""

	ElseIf GetDocumentType(oDoc) = "simpress" Then

		' We are in an Impress doc, set default preferences
		sEqSize = glb_sImpressSize
		sEqType = glb_sImpressEqType
		sEqLatexCode = ""

	ElseIf GetDocumentType(oDoc) = "sdraw" Then

		' We are in an Draw doc, set default preferences
		sEqSize = glb_sDrawSize
		sEqType = glb_sDrawEqType
		sEqLatexCode = ""

	Else

		Msgbox GetDocumentType(oDoc) & _(": document type not supported by TexMaths")
		Exit Sub

	End If

	' If there is something selected, then we are in the edit equation mode
	' and we have to get the equation attributes (font size, equation type, LaTeX code)
	EditEquation = FALSE
	Dialog = TRUE

	If Not isEmpty(oDocCtrl.selection()) Then

		Dim sDlgArg as String
		Dim sArray() as String
		Dim oSelection as Variant, oShape as Variant
		oSelection = oDocCtrl.getSelection()
		
		' We are in a Writer document
		If GetDocumentType(oDoc) = "swriter" Then

			' For Writer, check if the selected objet is a SwXTextGraphicObject
	    	If oSelection.getImplementationName() = "SwXTextGraphicObject" Then
		
				oShape = oSelection
				sDlgArg = ReadAttributes(oShape)
				
				If sDlgArg = "" Then
	
	    			Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
	    			Exit Sub  			

	    		End If
	       		EditEquation = TRUE

				sArray()=Split(sDlgArg,"§")

				' Pre v0.39 equation
				If Ubound(sArray) = 2 Then
				
		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
				
				ElseIf Ubound(sArray) = 5 Then

		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
		    		sEqFormat=sArray(3)
		    		sEqDPI=sArray(4)
		    		sEqTransp=sArray(5)
		    		sEqName=""

				ElseIf Ubound(sArray) = 6 Then

		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
		    		sEqFormat=sArray(3)
		    		sEqDPI=sArray(4)
		    		sEqTransp=sArray(5)
		    		sEqName=sArray(6)
				
				Else
				
	    			Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
	    			Exit Sub  			
				
				End If
	    		
	   			glb_sStatus = _("Edit equation...")	    		
	    		TexMathsDialog(sEqSize, sEqType, sEqLatexCode, sEqFormat, sEqDPI, sEqTransp, sEqName)
	    		Exit Sub
	    		
			' Selected objet is a SvxShapeCollection
	    	ElseIf oSelection.getImplementationName() = "com.sun.star.drawing.SvxShapeCollection" Then
		
				oShape = oSelection.getByIndex(0)
				sDlgArg = ReadAttributes(oShape)
				
				If sDlgArg = "" Then
	
	    			Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
	    			Exit Sub  			
	
	    		End If
	       		EditEquation = TRUE

				sArray()=Split(sDlgArg,"§")

				' Pre v0.39 equation
				If Ubound(sArray) = 2 Then
				
		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
				
				ElseIf Ubound(sArray) = 5 Then

		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
		    		sEqFormat=sArray(3)
		    		sEqDPI=sArray(4)
		    		sEqTransp=sArray(5)
		    		sEqName=""
				
				ElseIf Ubound(sArray) = 6 Then

		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
		    		sEqFormat=sArray(3)
		    		sEqDPI=sArray(4)
		    		sEqTransp=sArray(5)
		    		sEqName=sArray(6)

				Else
				
	    			Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
	    			Exit Sub  			
				
				End If
				    		
	   			glb_sStatus = _("Edit equation...")	    		
	    		TexMathsDialog(sEqSize, sEqType, sEqLatexCode, sEqFormat, sEqDPI, sEqTransp, sEqName)
	    		Exit Sub


			' Selected object is Text
			ElseIf oSelection.supportsService("com.sun.star.text.TextRanges") Then

				' Call MakeEquation without dialog box
				Dim oSel as Variant		
      			oSel = oSelection.getByIndex(0)
      			If oSel.supportsService("com.sun.star.text.TextRange") And oSel.String <> "" Then
     					
   					Dialog = FALSE

		 			' Find if we allow an extra CR in MakeEquation
		 			Dim oViewCursor as Variant
		 			Dim oCursor as Variant
		 			oViewCursor =  oDocCtrl.ViewCursor
		 			
		 			oCursor =  GetLeftMostCursor(oSel)
					oViewCursor.gotoRange(oCursor, FALSE)
	 	 			
	 				oCursor =  GetRightMostCursor(oSel)
					oViewCursor.gotoRange(oCursor, TRUE)
	
	 				' Generate LaTeX equation
	 				MakeEquation(CInt(sEqSize), sEqType, oSel.String, sEqFormat, sEqDPI, sEqTransp)	      					
	 				Exit Sub
      				
      			End If
      				
	    	End If
	
		' We are in an Impress or Draw document
		ElseIf GetDocumentType(oDoc) = "simpress" Or  GetDocumentType(oDoc) = "sdraw" Then
		
			' For Impress or Draw, check if the selected object is in a SvxShapeCollection
			If oSelection.getImplementationName() = "com.sun.star.drawing.SvxShapeCollection" Then
			
				oShape = oSelection.getByIndex(0)
				sDlgArg = ReadAttributes(oShape)
				If sDlgArg = "" Then
				
    				Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
	   				Exit Sub
	    					
	    		End If
				oShapePosition = oShape.position()
	       		EditEquation = TRUE
	    		
				sArray()=Split(sDlgArg,"§")

				' Pre v0.39 equation
				If Ubound(sArray) = 2 Then
				
		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
				
				ElseIf Ubound(sArray) = 5 Then

		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
		    		sEqFormat=sArray(3)
		    		sEqDPI=sArray(4)
		    		sEqTransp=sArray(5)
		    		sEqName=""

				ElseIf Ubound(sArray) = 6 Then

		    		sEqSize=sArray(0)
		    		sEqType=sArray(1)
		    		sEqLatexCode=sArray(2)
		    		sEqFormat=sArray(3)
		    		sEqDPI=sArray(4)
		    		sEqTransp=sArray(5)
		    		sEqName=sArray(6)
				
				Else
				
	    			Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
	    			Exit Sub  			
				
				End If
   		
	   			glb_sStatus = _("Edit equation...")
	    		TexMathsDialog(sEqSize, sEqType, sEqLatexCode, sEqFormat, sEqDPI, sEqTransp, sEqName)
	    		Exit Sub
	
	    	' Selected object is text within a frame
	    	ElseIf oSelection.getImplementationName() = "SvxUnoTextCursor" Then

    			Msgbox _("The selected object is not a TexMaths equation...") & chr(10) &_
	    				     	 _("Please unselect it and call the macro again...")
    			Exit Sub
    			
    		End If

		End If

	End If

	' Open the dialog box with the default options
	TexMathsDialog(sEqSize, sEqType, sEqLatexCode, sEqFormat, sEqDPI, sEqTransp, sEqName)

End Sub


' Set configuration
Sub SetConfig

	' Initialise state string
	glb_sStatus = ""
	
	' Set default system configuration (pkg, user and tmp paths)
	DefaultSysConfig()

	' Set default equation preferences (image format, equation type, font size)
	DefaultPrefs()

	' Apply saved config if it exists
	If FileExists(glb_sUserPath & "TexMaths.cfg" ) Then ReadPrefsFile()
	
	' Set default preamble
	DefaultPreamble()

	' Try to read a stored LaTeX preamble
	ReadPreamble()
	
End Sub


' This is the core macro! It is called by the dialog box
' with the sEqCode, sEqType, iEqSize, sEqFormat, sEqDPI and sEqTransp variables
' It makes the image and inserts it in the document
Sub MakeEquation(iEqSize as Integer, sEqType as String, sEqCode as String, sEqFormat as String, sEqDPI as String, sEqTransp as String)

	Dim iNumber as Integer
	Dim cURL as String, sShellArg as String, sMsg as String, sLatexCode as String, sShellCommand as String, sEqName as String

	' Save initial clipboard content because it will be lost when pasting the image
	Dim sClipContent as String
	sClipContent = ClipboardToText()

	' We first test if there is LateX code
	If sEqCode = "" Then 

		Msgbox _("Please enter some LaTeX code...")
		Exit Sub

	End If

	' Some environments only work in latex mode 
	If IsPrefixString("\begin{align",sEqCode) Or IsPrefixString("\begin{eqnarray",sEqCode) Or IsPrefixString("\begin{gather",sEqCode) _
	Or isPrefixString("\begin{flalign",sEqCode) Or IsPrefixString("\begin{multline",sEqCode) Then
		sEqType = "latex" 
		
		' Don't do this if the object is not defined 	
		If Not isNull(oDlgMain) Then
			oDlgMain.getControl("TypeLatex").setState(1)
		End If
	End If
	
	
	If sEqType = "inline"  Then
		
		sLatexCode = "\newsavebox{\eqbox}" & chr(10) &_
		"\newlength{\width}" & chr(10) &_
		"\newlength{\height}" & chr(10) &_
		"\newlength{\depth}" & chr(10) & chr(10) &_
		"\begin{lrbox}{\eqbox}" & chr(10) &_
		"{$ " & sEqCode & " $}" & chr(10) &_
		"\end{lrbox}" & chr(10) & chr(10) &_
		"\settowidth {\width}  {\usebox{\eqbox}}" & chr(10) &_
		"\settoheight{\height} {\usebox{\eqbox}}" & chr(10) &_
		"\settodepth {\depth}  {\usebox{\eqbox}}" & chr(10) &_
		"\newwrite\file" & chr(10) &_
		"\immediate\openout\file=\jobname.bsl" & chr(10) &_
		"\immediate\write\file{Depth = \the\depth}" & chr(10) &_
		"\immediate\write\file{Height = \the\height}" & chr(10) &_
		"\addtolength{\height} {\depth}" & chr(10) &_
		"\immediate\write\file{TotalHeight = \the\height}" & chr(10) &_
		"\immediate\write\file{Width = \the\width}" & chr(10) &_
		"\closeout\file" & chr(10) &_
		"\begin{document}" & chr(10) &_
		"\usebox{\eqbox}" & chr(10) &_
		"\end{document}" & chr(10)
		
	ElseIf sEqType = "display"  Then
		
		sLatexCode = "\newsavebox{\eqbox}" & chr(10) &_
		"\newlength{\width}" & chr(10) &_
		"\newlength{\height}" & chr(10) &_
		"\newlength{\depth}" & chr(10) & chr(10) &_
		"\begin{lrbox}{\eqbox}" & chr(10) &_
		"{$\displaystyle " & sEqCode & " $}" & chr(10) &_
		"\end{lrbox}" & chr(10) & chr(10) &_
		"\settowidth {\width}  {\usebox{\eqbox}}" & chr(10) &_
		"\settoheight{\height} {\usebox{\eqbox}}" & chr(10) &_
		"\settodepth {\depth}  {\usebox{\eqbox}}" & chr(10) &_
		"\newwrite\file" & chr(10) &_
		"\immediate\openout\file=\jobname.bsl" & chr(10) &_
		"\immediate\write\file{Depth = \the\depth}" & chr(10) &_
		"\immediate\write\file{Height = \the\height}" & chr(10) &_
		"\addtolength{\height} {\depth}" & chr(10) &_
		"\immediate\write\file{TotalHeight = \the\height}" & chr(10) &_
		"\immediate\write\file{Width = \the\width}" & chr(10) &_
		"\closeout\file" & chr(10) &_
		"\begin{document}" & chr(10) &_
		"\usebox{\eqbox}" & chr(10) &_
		"\end{document}" & chr(10)

	ElseIf sEqType = "latex" Then
	
		sLatexCode = sEqCode
		
	End If
	
	' Open service file and an output stream
	Dim oFileAccess as Variant, oTextStream as Variant
	oFileAccess = createUnoService("com.sun.star.ucb.SimpleFileAccess")
	oTextStream  = createUnoService("com.sun.star.io.TextOutputStream")

	' Create the LaTeX file with the LatexCode
	cURL = ConvertToURL( glb_sTmpPath & "tmpfile.tex" )
	If oFileAccess.exists( cURL ) Then oFileAccess.kill( cURL )
    oTextStream.setOutputStream(oFileAccess.openFileWrite(cURL))
	
	If sEqType = "latex" Then
	
		oTextStream.writeString( _
		    	"\documentclass[10pt]{article}" & chr(10) &_
		    	glb_sPreamble & chr(10) & chr(10) &_
		    	"\pagestyle{empty}" & chr(10) &_
		    	"\begin{document}" & chr(10) &_
		    	sLatexCode & chr(10) &_
		    	"\end{document}" )
	
	Else
	
		oTextStream.writeString( _
		    	"\documentclass[10pt]{article}" & chr(10) &_
		    	glb_sPreamble & chr(10) & chr(10) &_
		    	"\pagestyle{empty}" & chr(10) &_
		    	sLatexCode
	
	End If

	' Close the file
    oTextStream.closeOutput()
    
    ' Test the existence of the LaTeX file...
    If CheckFile( glb_sTmpPath & "tmpfile.tex" , _
    		_("The file ") & ConvertFromURL(glb_sTmpPath) & _("tmpfile.tex cannot be created") & chr(10) & _
			_("Please check your installation...") ) Then 

		ConfigDialog()
		Exit Sub

	End If
	
	' Windows
	If getGUIType() = 1 Then

		sShellCommand = GetScriptPath()	
		sShellArg = sEqFormat & " "  & sEqDPI & " "  & sEqTransp & " " & winPath(glb_sTmpPath)

	' Linux or Mac OS X
	Else 					
		
		sShellCommand = "/bin/bash"
		sShellArg = "'" & ConvertFromURL(GetScriptPath()) & "' " & sEqFormat &_
				    " " & sEqDPI & " " & sEqTransp & " '"  & ConvertFromURL(glb_sTmpPath) & "'"

	End If

    ' Remove Latex output file
	cURL = ConvertToURL( glb_sTmpPath & "tmpfile.out" )
	If oFileAccess.exists( cURL ) Then oFileAccess.kill( cURL )

	' Call the script
	Shell(sShellCommand, 2, sShellArg, TRUE)

	' Check the result
 	If Not FileExists(glb_sTmpPath & "tmpfile.dvi") _
 	   and Not FileExists(glb_sTmpPath & "tmpfile.out")  Then
		Msgbox ( _("No file created in the directory:") & _
  		       chr(10) & ConvertFromURL(glb_sTmpPath) )
  		Exit Sub

 	ElseIf Not FileExists(glb_sTmpPath & "tmpfile.dvi") _
 	       and FileExists(glb_sTmpPath & "tmpfile.out")  Then ' Latex error

  		PrintFile("tmpfile.out")
  		Exit Sub

 	ElseIf CheckFile(glb_sTmpPath & "tmpfile." & sEqFormat,_
   		_("Script error: the dvi file was not converted to ") & sEqFormat & "! " & chr(10) & chr(10) &_
   		_("Please check your system configuration...") ) Then Exit Sub
 	End If

	' Image size is 0
	If FileExists(glb_sTmpPath & "tmpfile.bsl") Then
		If GetVertShift() = 0 Then Exit Sub
	End If

	' If we arrive here, the compilation succeeded. We can close the dialog
	If Dialog Then
		oDlgMain.endExecute()
		SavePosSize()
	End If

	' Create the Controller and dispatcher for current document
	Dim oDoc as Variant, oDocCtrl as Variant, oDispatcher as Variant, oGraphic as Variant, oShapeSize as Variant
	oDoc = ThisComponent
	oDocCtrl = oDoc.getCurrentController()
	oDispatcher = createUnoService("com.sun.star.frame.DispatchHelper")


	' ================== Current document is a Writer document ==================
	If GetDocumentType(oDoc) = "swriter" Then

		Dim oViewCursor as Variant, oCursor as Variant
		Dim AnchorType as Integer

		' If there is already an equation image, remove it
		If EditEquation Then

			' Select image (ensuring compatibility with previous TexMaths versions)
			Dim oSelection as Variant
			On Error Goto SelectionError
			oSelection = oDocCtrl.getSelection().GetByIndex(0)		
			
			' Get selected image anchor
			Dim oAnchor as Variant
			oAnchor = oSelection.getAnchor()
			AnchorType = oSelection.AnchorType

			' Save image position for further use
			Dim oImgPosition as Variant
			oImgPosition = oSelection.getPosition()

			' Delete the selected image
		 	oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:Delete", "", 0, Array())

		 End If
		 
		' Set vertical alignement to middle when Word compatibility is requested
		If glb_WordVertAlign = TRUE Then
			
			dim args4(0) as new com.sun.star.beans.PropertyValue
			args4(0).Name = "VerticalParagraphAlignment"
			args4(0).Value = 3		
			oDispatcher.executeDispatch(oDocCtrl.Frame, ".uno:VerticalParagraphAlignment", "", 0, args4())
		
		End If
		
		' Set text cursor position to the view cursor position
		oViewCursor = oDocCtrl.ViewCursor
		oCursor = oViewCursor.Text.createTextCursorByRange(oViewCursor)
		oCursor.gotoRange(oViewCursor,FALSE)
								
		' Import the new image into the clipboard
   		ImportGraphicIntoClipboard(ConvertToURL( glb_sTmpPath & "tmpfile."& sEqFormat), sEqFormat, sEqDPI, sEqTransp)	
	
		' Paste image to the current document
		oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:Paste", "", 0, Array())
	
	  	' Select image
	   	oGraphic = oDocCtrl.getSelection().GetByIndex(0)

		' Set the graphic object name
		oGraphic.Name = oDlgMain.getControl("EquationNameText").getText()
		sEqname = oGraphic.Name

		' Scale image
		oShapeSize = oGraphic.Size
		oShapeSize.Width = oShapeSize.Width * (iEqSize / 10)
		oShapeSize.Height = oShapeSize.Height * (iEqSize / 10)
		oGraphic.Size = oShapeSize	

		' In edit mode, anchor the image as it was
		If EditEquation Then
		
			Select Case AnchorType

				Case ANCHOR_TO_PARA
					oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:SetAnchorToPara", "", 0, Array())
					oGraphic.setPosition(oImgPosition)
				
				Case ANCHOR_TO_CHAR
					oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:SetAnchorToChar", "", 0, Array())
					' Don't position image in this case
				
				Case ANCHOR_TO_PAGE
					oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:SetAnchorToPage", "", 0, Array())
					oGraphic.setPosition(oImgPosition)					
				
				Case ANCHOR_AT_CHAR
					oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:SetAnchorAtChar", "", 0, Array())
					oGraphic.setPosition(oImgPosition)
			
			End Select
		
		' New equations are anchored to char
		Else
		
			AnchorType = ANCHOR_TO_CHAR
			oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:SetAnchorToChar", "", 0, Array())
			
		End If
	
		 ' Set some image properties
		oGraphic.TopMargin = 0
		oGraphic.BottomMargin = 0
		oGraphic.VertOrient = 0

		' Adjust vertical position for Inline or Display equations when image is anchored to char
		If AnchorType = ANCHOR_TO_CHAR and ( sEqtype = "inline" or sEqType = "display" ) Then
			
			' Get vertical shift coefficient
			Dim coef as Double
			coef = getVertShift()
				
			' Adjust the vertical position to the baseline
			oGraphic.VertOrientPosition = - coef * oShapeSize.Height

		End If

		' Set image attributes (size, type LaTeX code) for further editing
		SetAttributes( oGraphic, iEqSize, sEqType, sEqCode, sEqFormat, sEqDPI, sEqTransp, sEqName )

		' Trick used to ensure the image is located at the cursor position 
		If AnchorType = ANCHOR_TO_CHAR Then

			oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:Cut", "", 0, Array())
			oViewCursor.gotoRange(oCursor, FALSE)
  			oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:Paste", "", 0, Array())

		Else

			' Deselect image
			oDispatcher.executeDispatch(oDocCtrl.Frame, ".uno:Escape", "", 0, Array())

		End If

		' Reposition the view cursor
		oViewCursor.gotoRange(oCursor, FALSE)



	' ================== Current document is an Impress or Draw document ==================
	ElseIf GetDocumentType(oDoc) = "simpress" or GetDocumentType(oDoc) = "sdraw" Then
		
 		' Edit equation: remove old image
 		If EditEquation Then 
 			
 			' Fill in reference to original shape (used to keep animations)
			Dim oOriginalShape as Variant
			oOriginalShape = oDocCtrl.getSelection().getByIndex(0)
 			oDispatcher.executeDispatch(oDocCtrl.Frame,".uno:Cut","", 0, Array() )
 		
 		End If
		
		' Import the new image to the clipboard
   		ImportGraphicIntoClipboard(ConvertToURL( glb_sTmpPath & "tmpfile." & sEqFormat), sEqFormat, sEqDPI, sEqTransp)

		' Paste image to the current document
		oDispatcher.executeDispatch( oDocCtrl.Frame, ".uno:Paste", "", 0, Array())

		' Select image
		oGraphic = oDocCtrl.getSelection().getByIndex(0)

		' Set the graphic object name
		oGraphic.Name = oDlgMain.getControl("EquationNameText").getText()
		sEqname = oGraphic.Name

		' Edit equation: set its position equal to the previous one
		If EditEquation Then
			oGraphic.setPosition(oShapePosition)
		
		' New equation: Position the image at the center of the visible area
		Else
		
			Dim oPosition as Variant
			oPosition = createUnoStruct( "com.sun.star.awt.Point" ) 
			oPosition.X = oDocCtrl.VisibleArea.X + oDocCtrl.VisibleArea.Width / 2 - (oGraphic.Size.Width*iEqSize/10) / 2
			oPosition.Y = oDocCtrl.VisibleArea.Y + oDocCtrl.VisibleArea.Height / 2	- (oGraphic.Size.Height* iEqSize/10) / 2
			oGraphic.setPosition(oPosition)

		End If

		' Scale the image
		oShapeSize = oGraphic.Size
		oShapeSize.Width = oShapeSize.Width * iEqSize / 10
		oShapeSize.Height = oShapeSize.Height * iEqSize / 10
		oGraphic.Size = oShapeSize
		
		' Edit equation: Transfer animations from old shape to new shape
 		If EditEquation and GetDocumentType(oDoc) = "simpress" Then
			TransferAnimations(oDocCtrl.getCurrentPage(), oOriginalShape, oGraphic) 			
 		End If

		' Set image attributes (size, type LaTeX code) for further editing
		SetAttributes( oGraphic, iEqSize, sEqType, sEqCode, sEqFormat, sEqDPI, sEqTransp, sEqName)

	End If
	
	' If Writer is installed, restore initial clipboard content
	If ComponentInstalled( "Writer" ) Then
		TextToClipboard(sClipContent)
	End If

	Exit Sub

	' To ensure compatibility with previous TexMaths versions
	SelectionError:
		oSelection = oDocCtrl.getSelection
		Resume Next

End Sub


