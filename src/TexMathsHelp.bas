'
'    TexMathsHelp
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'
'	 Macros used to create and manage the Help dialog




' Force variable declaration
Option Explicit

Private oDlgHelp as Variant
Private oL as Variant
Private closed as Boolean


' Main subroutine
Sub Main()

	DefaultSysConfig()
	HelpDialog()

	' Loop for non modal dialog
	closed = FALSE
	While(closed = FALSE)
		Wait 1000
	Wend

End Sub


' Create and show non modal dialog
Sub HelpDialog()

	' Load the library TexMaths
	DialogLibraries.LoadLibrary("TexMaths")

	' Create the dialog object
	oDlgHelp = createUnoDialog( DialogLibraries.GetByName("TexMaths").GetByName("TexMathsHelp_GUI") )

	' Window listener
	oL = createUnoListener("HelpWindow_","com.sun.star.awt.XTopWindowListener")
	oDlgHelp.addTopWindowListener(oL)

	' Load help text, depending on the locale 
	Dim sHelpPath as String, sFilePath as String, sMsg as String
	sHelpPath = "help/help." & GetLocale()
	sFilePath = ConvertToURL( glb_sPkgPath & sHelpPath )
	If Not FileExists( sFilePath ) Then	sHelpPath =  "help/help.en"
	sMsg = ReadTextFileUtf8(sHelpPath, glb_sPkgPath)
	sMsg = chr(10) & " TexMaths " & GetVersion() & chr(10) & sMsg
	oDlgHelp.getControl("HelpText").setText(sMsg)

	' Dialog strings for translation
	oDlgHelp.setTitle( _("TexMaths Help") )
	oDlgHelp.getControl("CloseButton").Model.Label = _("Close")
	oDlgHelp.getControl("CloseButton").Model.HelpText = _("Close window")

	' Show the dialog window
 	oDlgHelp.setvisible(true)
   
End Sub


' Close non modal dialog
Sub CloseHelp()

	oDlgHelp.setVisible(false)
	closed = TRUE

End Sub


' It's recommended to implement all unneeded methods, called by a listener, as empty routines
' including "disposing" from parent-interface com.sun.star.lang.XEventListener

'Is invoked when a window is activated
Sub HelpWindow_windowActivated(oEv as Variant)
End Sub

'Is invoked when a window is de-activated
Sub HelpWindow_windowDeactivated(oEv)
End Sub

'Invoked when a window has been opened
Sub HelpWindow_windowOpened(oEv as Variant)
End Sub

'Close window using the menu bar close button
'Invoked when a window is in the process of being closed
Sub HelpWindow_windowClosing(oEv as Variant)
	oDlgHelp.setVisible(false)
	closed = TRUE
End Sub

' Is invoked when a window has been closed
Sub HelpWindow_windowClosed(oEv as Variant)
End Sub

' Is invoked when a window is iconified
Sub HelpWindow_windowMinimized(oEv as Variant)
End Sub

' Is invoked when a window is de-iconified
Sub HelpWindow_windowNormalized(oEv as Variant)
End Sub

Sub HelpWindow_disposing(oEv as Variant)
End Sub

