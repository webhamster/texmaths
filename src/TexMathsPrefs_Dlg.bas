'
'    TexMathsPrefs_Dlg
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

'	 Macros used to create and manage the Preferences dialog

' Force variable declaration
Option Explicit

' Dialog declaration
Private oDlgPrefs as Variant


' Create and display dialog
Sub PrefsDialog()

	' Load the library TexMaths
	DialogLibraries.LoadLibrary("TexMaths")

	' Create the dialog object
	oDlgPrefs = createUnoDialog( DialogLibraries.GetByName("TexMaths").GetByName("TexMathsPrefs_GUI") )

	' Set the combo box one time
	oDlgPrefs.getControl("Format").addItems(Array("png","svg"),0)
	oDlgPrefs.getControl("GraphicDPI").addItems(Array("72","96","120","150","200","300","600","1200"),0)
	oDlgPrefs.getControl("ImpressSize").addItems(Array("6","7","8","9","10","10.5","11","12","13","14","15","16","18","20","22","24","26","28","32","36","40","44","48","54","60","66","72","80","88","96"),0)
	oDlgPrefs.getControl("WriterSize").addItems(Array("6","7","8","9","10","10.5","11","12","13","14","15","16","18","20","22","24","26","28","32","36","40","44","48","54","60","66","72","80","88","96"),0)
	oDlgPrefs.getControl("DrawSize").addItems(Array("6","7","8","9","10","10.5","11","12","13","14","15","16","18","20","22","24","26","28","32","36","40","44","48","54","60","66","72","80","88","96"),0)

	' Dialog strings for translation
	oDlgPrefs.setTitle( _("TexMaths Preferences") )
	oDlgPrefs.getControl("ImageFrame").Model.Label = _("Image parameters")
	oDlgPrefs.getControl("DefaultButton").Model.Label = _("Default")
	oDlgPrefs.getControl("DefaultButton").Model.HelpText = _("Load default preferences")
	oDlgPrefs.getControl("LoadButton").Model.Label = _("Load")
	oDlgPrefs.getControl("LoadButton").Model.HelpText = _("Load saved preferences")
	oDlgPrefs.getControl("SaveButton").Model.Label = _("Save")
	oDlgPrefs.getControl("SaveButton").Model.HelpText = _("Apply and save current preferences")
	oDlgPrefs.getControl("WriterSize").Model.HelpText = _("Writer font size")
	oDlgPrefs.getControl("ImpressSize").Model.HelpText = _("Impress font size")
	oDlgPrefs.getControl("DrawSize").Model.HelpText = _("Draw font size")
	oDlgPrefs.getControl("PointsLabel1").Model.Label = _("points")
	oDlgPrefs.getControl("PointsLabel2").Model.Label = _("points")
	oDlgPrefs.getControl("PointsLabel3").Model.Label = _("points")
	oDlgPrefs.getControl("WriterDisplay").Model.HelpText = _("Inline display equation")
	oDlgPrefs.getControl("ImpressDisplay").Model.HelpText = _("Inline display equation")
	oDlgPrefs.getControl("DrawDisplay").Model.HelpText = _("Inline display equation")
	oDlgPrefs.getControl("WriterInline").Model.HelpText = _("Inline equation")
	oDlgPrefs.getControl("ImpressInline").Model.HelpText = _("Inline equation")
	oDlgPrefs.getControl("DrawInline").Model.HelpText = _("Inline equation")
	oDlgPrefs.getControl("WriterLatex").Model.HelpText = _("Regular LaTeX code")
	oDlgPrefs.getControl("ImpressLatex").Model.HelpText = _("Regular LaTeX code")
	oDlgPrefs.getControl("DrawLatex").Model.HelpText = _("Regular LaTeX code")
	oDlgPrefs.getControl("FormatLabel").Model.Label = _("Format")
	oDlgPrefs.getControl("Format").Model.HelpText = _("Image format")
	oDlgPrefs.getControl("GraphicDPI").Model.HelpText = _("Image resolution")
	oDlgPrefs.getControl("Transparency").Model.Label = _("Transparency")
	oDlgPrefs.getControl("Transparency").Model.HelpText = _("Toggle image transparency")

	SetDialogPrefsValues(FALSE)

 	' Display the dialog, this routine call does not return until the dialog is dismissed
	oDlgPrefs.Execute()

End Sub


' Set dialog values
Sub SetDialogPrefsValues(ForceDefault as Boolean)

	' The button is activated if the preferences file exists
	If FileExists(glb_sUserPath & "TexMaths.cfg" ) Then 
		oDlgPrefs.getControl("LoadButton").setEnable(TRUE)
		
		If ForceDefault = FALSE Then ReadPrefsFile() Else DefaultPrefs()

	Else
		oDlgPrefs.getControl("LoadButton").setEnable(FALSE)
		DefaultPrefs()
	End If

	' Image
	oDlgPrefs.getControl("Format").setText(glb_sFormat)
	oDlgPrefs.getControl("GraphicDPI").setText(glb_sGraphicDPI)
	If glb_sTransparency = "TRUE" Then
		oDlgPrefs.getControl("Transparency").setState(1)
	Else
		oDlgPrefs.getControl("Transparency").setState(0)
	End If

	' Writer preferences
	oDlgPrefs.getControl("WriterSize").setText(glb_sWriterSize)
	If glb_sWriterEqType = "display" Then oDlgPrefs.getControl("WriterDisplay").setState(1)
	If glb_sWriterEqType = "inline"  Then oDlgPrefs.getControl("WriterInline").setState(1)
	If glb_sWriterEqType = "latex"    Then oDlgPrefs.getControl("WriterLatex").setState(1)

	' Impress preferences
	oDlgPrefs.getControl("ImpressSize").setText(glb_sImpressSize)
	If glb_sImpressEqType = "display" Then oDlgPrefs.getControl("ImpressDisplay").setState(1)
	If glb_sImpressEqType = "inline"  Then oDlgPrefs.getControl("ImpressInline").setState(1)
	If glb_sImpressEqType = "latex"    Then oDlgPrefs.getControl("ImpressLatex").setState(1)

	' Draw preferences
	oDlgPrefs.getControl("DrawSize").setText(glb_sDrawSize)
	If glb_sDrawEqType = "display" Then oDlgPrefs.getControl("DrawDisplay").setState(1)
	If glb_sDrawEqType = "inline"  Then oDlgPrefs.getControl("DrawInline").setState(1)
	If glb_sDrawEqType = "latex"    Then oDlgPrefs.getControl("DrawLatex").setState(1)
	

	' Get the dvipng and svisvgm paths saved in the registry
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)	

	' Disable svg and force png if dvisvgm path is empty
	If oSystemInfo.DvipngPath = "" Then
		
		glb_sFormat = "svg" 
		oDlgPrefs.getControl("Format").setText("svg")
		oDlgPrefs.getControl("Format").setEnable(FALSE)
	
	End If
			
	' Disable svg and force png if dvisvgm path is empty
	If oSystemInfo.DvisvgmPath = "" Then
		
		glb_sFormat = "png" 
		oDlgPrefs.getControl("Format").setText("png")
		oDlgPrefs.getControl("Format").setEnable(FALSE)
	
	End If

	' Disable dpi if svg file format
	If glb_sFormat = "svg" Then 
		oDlgPrefs.getControl("GraphicDPI").setEnable(FALSE)
	Else 
		oDlgPrefs.getControl("GraphicDPI").setEnable(TRUE)
	End If

End Sub


' Set image format and enable or disable dpi according to the image format
Sub Format_Mod()

	glb_sFormat = oDlgPrefs.getControl("Format").getText()
	If glb_sFormat = "svg" Then 
		oDlgPrefs.getControl("GraphicDPI").setEnable(FALSE)
	Else 
		oDlgPrefs.getControl("GraphicDPI").setEnable(TRUE)
	End If

End Sub



' Set image dpi
Sub Dpi_Mod()

	glb_sGraphicDPI = oDlgPrefs.getControl("GraphicDPI").getText()

End Sub


' Set image transparency
Sub Trans_Mod()

	If oDlgPrefs.getControl("Transparency").getState() Then
		glb_sTransparency = "TRUE"
	Else
		glb_sTransparency = "FALSE"
	End If	

End Sub


' Save preferences button clicked
Sub SavePrefsButton_Clicked()

	' Apply preferences
	ApplyPrefs()

	' Save preferences and close dialog
	SavePrefs()
	oDlgPrefs.endExecute()

End Sub


' Load default preferences
Sub DefaultPrefsButton_Clicked()

	' Load default preferences
	SetDialogPrefsValues(TRUE)
	
End Sub


' Load preferences
Sub LoadPrefsButton_Clicked()

	' We arrive here if the file exists
	ReadPrefsFile()
	SetDialogPrefsValues(FALSE)

End Sub


' Apply preferences
Sub ApplyPrefs()

	' Set the preferences variables
	glb_sFormat = oDlgPrefs.getControl("Format").getText()
	glb_sGraphicDPI = oDlgPrefs.getControl("GraphicDPI").getText()
	If oDlgPrefs.getControl("Transparency").getState() Then
		glb_sTransparency = "TRUE"
	Else
		glb_sTransparency = "FALSE"
	End If

	If glb_sFormat = "svg" Then 
		oDlgMain.getControl("GraphicDPI").setEnable(FALSE)
	Else 
		oDlgMain.getControl("GraphicDPI").setEnable(TRUE)
	End If

	glb_sWriterSize =  oDlgPrefs.getControl("WriterSize").getText()
	If oDlgPrefs.getControl("WriterDisplay").getState() Then glb_sWriterEqType = "display"
	If oDlgPrefs.getControl("WriterInline").getState() Then glb_sWriterEqType = "inline"
	If oDlgPrefs.getControl("WriterLatex").getState() Then glb_sWriterEqType = "latex"

	glb_sImpressSize =  oDlgPrefs.getControl("ImpressSize").getText()
	If oDlgPrefs.getControl("ImpressDisplay").getState() Then glb_sImpressEqType = "display"
	If oDlgPrefs.getControl("ImpressInline").getState() Then glb_sImpressEqType = "inline"
	If oDlgPrefs.getControl("ImpressLatex").getState() Then glb_sImpressEqType = "latex"

	' Draw preferences
	glb_sDrawSize =  oDlgPrefs.getControl("DrawSize").getText()
	If oDlgPrefs.getControl("DrawDisplay").getState() Then glb_sDrawEqType = "display"
	If oDlgPrefs.getControl("DrawInline").getState() Then glb_sDrawEqType = "inline"
	If oDlgPrefs.getControl("DrawLatex").getState() Then glb_sDrawEqType = "latex"

	' Update the fields of the main window depending on the document type
	oDlgMain.getControl("Format").setText(glb_sFormat)
	oDlgMain.getControl("GraphicDPI").setText(glb_sGraphicDPI)
	If glb_sTransparency = "TRUE" Then
		oDlgMain.getControl("Transparency").setState(1)
	Else
		oDlgMain.getControl("Transparency").setState(0)
	End If

	If GetDocumentType(ThisComponent) = "swriter" Then
		
		oDlgMain.getControl("Size").setText(glb_sWriterSize)
		If glb_sWriterEqType = "display" Then oDlgMain.getControl("TypeDisplay").setState(1)
		If glb_sWriterEqType = "inline" Then oDlgMain.getControl("TypeInline").setState(1)
		If glb_sWriterEqType = "latex" Then oDlgMain.getControl("TypeLatex").setState(1)
		
	ElseIf GetDocumentType(ThisComponent) = "simpress" Then
	
		oDlgMain.getControl("Size").setText(glb_sImpressSize)
		If glb_sImpressEqType = "display" Then oDlgMain.getControl("TypeDisplay").setState(1)
		If glb_sImpressEqType = "inline" Then oDlgMain.getControl("TypeInline").setState(1)
		If glb_sImpressEqType = "latex" Then oDlgMain.getControl("TypeLatex").setState(1)
	
	ElseIf GetDocumentType(ThisComponent) = "sdraw" Then
	
		oDlgMain.getControl("Size").setText(glb_sDrawSize)
		If glb_sDrawEqType = "display" Then oDlgMain.getControl("TypeDisplay").setState(1)
		If glb_sDrawEqType = "inline" Then oDlgMain.getControl("TypeInline").setState(1)
		If glb_sDrawEqType = "latex" Then oDlgMain.getControl("TypeLatex").setState(1)
	
	Else
	
		Msgbox GetDocumentType(oDoc) & _(": document type not supported by TexMaths")
		Exit Sub

	End If
	
	' Save into doc if necessary and update Log
	DisplayStatus ( _("Preferences applied and saved...") )

End Sub


' Save preferences
Sub SavePrefs()

	' Open service file and an output stream
	Dim oFileAccess as Variant, oTextStream as Variant
	oFileAccess = createUnoService("com.sun.star.ucb.SimpleFileAccess")
	oTextStream = createUnoService("com.sun.star.io.TextOutputStream")

	' Save the general options
	Dim cURL as String
	cURL = ConvertToURL( glb_sUserPath & "TexMaths.cfg" )
	If oFileAccess.exists( cURL ) Then oFileAccess.kill( cURL )
    oTextStream.setOutputStream(oFileAccess.openFileWrite(cURL))
    oTextStream.writeString(_
    			"# TexMaths config file" & chr(10) &_
				"#" &chr(10)  ) 
    oTextStream.writeString( Preferences() & chr(10))

	oTextStream.closeOutput()

End Sub

