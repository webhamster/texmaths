'
'    TexMathsSysConfig
'
'	 Copyright (C) 2012-2014 Roland Baudin (roland65@free.fr)
'    Based on the work of Geoffroy Piroux (gpiroux@gmail.com)
'
'    This program is free software; you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation; either version 2 of the License, or
'    (at your option) any later version.
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with this program; if not, write to the Free Software
'    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

' 	 Macros used to create and manage the Sysconfig dialog 

' Force variable declaration
Option Explicit


' Dialog declaration
Private oDlgSysConfig as Variant


' Main subroutine
Sub Main()

	' The Draw component is required
	If ComponentInstalled( "Draw" ) = FALSE Then

  		msgbox( _("Please install the Draw component to run TexMaths!") )
  		Exit Sub

	End If	 
 
 	DefaultSysConfig()
	SysConfigDialog()

End Sub


' Create and show the config dialog
Sub SysConfigDialog()

	' Load the library TexMaths
	DialogLibraries.LoadLibrary( "TexMaths" )
	
	' Create the dialog object
	oDlgSysConfig = createUnoDialog( DialogLibraries.GetByName("TexMaths").GetByName("TexMathsSysConfig_GUI") )
	
	' Dialog strings for translation
	oDlgSysConfig.setTitle( _("TexMaths System Configuration") )
	oDlgSysConfig.getControl("ShortcutFrame1").Model.Label = _("Equations Shortcuts")
	oDlgSysConfig.getControl("ShortcutFrame2").Model.Label = _("Numbered Equations Shortcut")
	oDlgSysConfig.getControl("SystempathsFrame").Model.Label = _("System Paths")
	oDlgSysConfig.getControl("AllFrame").Model.Label = _("All")
	oDlgSysConfig.getControl("Key1").Model.HelpText = _("Keyboard shortcut")
	oDlgSysConfig.getControl("Key2").Model.HelpText = _("Keyboard shortcut")
	oDlgSysConfig.getControl("Key3").Model.HelpText = _("Keyboard shortcut")
	oDlgSysConfig.getControl("Key4").Model.HelpText = _("Keyboard shortcut")
	oDlgSysConfig.getControl("LatexPath").Model.HelpText = _("Path of the latex program")
	oDlgSysConfig.getControl("DvipngPath").Model.HelpText = _("Path of the dvipng program")
	oDlgSysConfig.getControl("DvisvgmPath").Model.HelpText = _("Path of the dvisvgm program")
	oDlgSysConfig.getControl("SaveButton1").Model.Label = _("Save")
	oDlgSysConfig.getControl("SaveButton1").Model.HelpText = _("Save system configuration")
	oDlgSysConfig.getControl("SaveButton2").Model.Label = _("Save")
	oDlgSysConfig.getControl("SaveButton2").Model.HelpText = _("Save system configuration")
	oDlgSysConfig.getControl("SaveButton3").Model.Label = _("Save")
	oDlgSysConfig.getControl("SaveButton3").Model.HelpText = _("Save system configuration")
	oDlgSysConfig.getControl("NumLevelLabel").Model.Label = _("Numbering Level")
	oDlgSysConfig.getControl("NumLevelLabel").Model.HelpText = _("Chapter level number to be included in equation numbers")
	oDlgSysConfig.getControl("NumLevel").addItems(Array("0","1","2","3","4"),0)
	oDlgSysConfig.getControl("NumLevel").Model.HelpText = _("Level number")

	Dim str1 as String, str2 as String
	str1 =  _("Paths")
	str2 = _("Display the Paths tab")
	oDlgSysConfig.getControl("PathsButton1").Model.Label = str1
	oDlgSysConfig.getControl("PathsButton3").Model.Label = str1
	oDlgSysConfig.getControl("PathsButton2").Model.Label = str1
	oDlgSysConfig.getControl("PathsButton1").Model.HelpText = str2
	oDlgSysConfig.getControl("PathsButton2").Model.HelpText = str2
	oDlgSysConfig.getControl("PathsButton3").Model.HelpText = str2

	str1 =  _("Options")
	str2 =  _("Display the Options tab")
	oDlgSysConfig.getControl("OptionsButton1").Model.Label = str1
	oDlgSysConfig.getControl("OptionsButton2").Model.Label = str1
	oDlgSysConfig.getControl("OptionsButton3").Model.Label = str1
	oDlgSysConfig.getControl("OptionsButton1").Model.HelpText = str2
	oDlgSysConfig.getControl("OptionsButton2").Model.HelpText = str2
	oDlgSysConfig.getControl("OptionsButton3").Model.HelpText = str2

	str1 =  _("Shortcuts")
	str2 = _("Display the Shortcuts tab")
	oDlgSysConfig.getControl("ShortcutsButton1").Model.Label = str1
	oDlgSysConfig.getControl("ShortcutsButton2").Model.Label = str1
	oDlgSysConfig.getControl("ShortcutsButton3").Model.Label = str1
	oDlgSysConfig.getControl("ShortcutsButton1").Model.HelpText = str2
	oDlgSysConfig.getControl("ShortcutsButton2").Model.HelpText = str2
	oDlgSysConfig.getControl("ShortcutsButton3").Model.HelpText = str2

	oDlgSysConfig.getControl("NumberedEquationsFrame").Model.Label = _("Numbered Equations")
	oDlgSysConfig.getControl("CompatibilityFrame").Model.Label = _("Compatibility")
	oDlgSysConfig.getControl("VertAlignCheck").Model.Label = _("Vertical alignment for Word export")
	oDlgSysConfig.getControl("VertAlignCheck").Model.HelpText = _("Improve vertical alignment of Writer equations for Word export")
	oDlgSysConfig.getControl("BreakBeforeNumCheck").Model.Label = _("Paragraph break before numbered equations")
	oDlgSysConfig.getControl("BreakBeforeNumCheck").Model.HelpText = _("Add a paragraph break before numbered equations")
	oDlgSysConfig.getControl("BreakAfterNumCheck").Model.Label = _("Paragraph break after numbered equations")
	oDlgSysConfig.getControl("BreakAfterNumCheck").Model.HelpText = _("Add a paragraph break after numbered equations")
	oDlgSysConfig.getControl("CaptionLabel").Model.Label = _("Equation Caption")
	oDlgSysConfig.getControl("CaptionTextField").Model.HelpText = _("Display text before equation number (leave blank if no text desired)")

	Dim msg as String
	msg = _("The TexMaths macro uses some external programs to generate the equation images. Enter below the path of these programs. Note that only one of the dvisvgm or dvipng programs is required to generate equation images.")

	' Windows
	If getGUIType() = 1 Then

			oDlgSysConfig.getControl("Description").setText( msg & chr(10) & chr(10) &_
					_("Ex: For a standard MiKTeX 2.9 install on Windows, enter the following path into the latex.exe field:") & chr(10) &_
					"C:\Program Files\MiKTeX 2.9\miktex\bin"
	
			oDlgSysConfig.getControl("LatexLabel").setText( _("latex.exe (mandatory)") )
			oDlgSysConfig.getControl("DvipngLabel").setText( _("dvipng.exe (optional)") )
			oDlgSysConfig.getControl("DvisvgmLabel").setText( _("dvisvgm.exe (optional)") )
			oDlgSysConfig.getControl("CompatibilityFrame").Model.Height = 34		

	' Linux or Mac OS X
	Else
	
		' Mac OS X
		If GetUname() = "Darwin" Then
		
			oDlgSysConfig.getControl("Description").setText( msg & chr(10) & chr(10) &_
					_("Ex: For a standard LaTeX install on Mac OS, enter the following path into the latex field:") & chr(10) &_
					"/usr/texbin"
					
			oDlgSysConfig.getControl("LatexLabel").setText( _("latex (mandatory)") )
			oDlgSysConfig.getControl("DvipngLabel").setText( _("dvipng (optional)") )
			oDlgSysConfig.getControl("DvisvgmLabel").setText( _("dvisvgm (optional)") )
		
		' Linux
		Else
		
			oDlgSysConfig.getControl("Description").setText( msg & chr(10) & chr(10) &_
					_("Ex: For a standard LaTeX install on Linux, enter the following path into the latex field:") & chr(10) &_
					"/usr/bin"
					
			oDlgSysConfig.getControl("LatexLabel").setText( _("latex (mandatory)") )
			oDlgSysConfig.getControl("DvipngLabel").setText( _("dvipng (optional)") )
			oDlgSysConfig.getControl("DvisvgmLabel").setText( _("dvisvgm (optional)") )
		
		End If
			
	End If

	' Fill the path values from the registry
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess("/ooo.ext.texmaths.Registry/SystemInfo", TRUE)
	oDlgSysConfig.getControl("LatexPath").setText(ConvertFromURL(oSystemInfo.LatexPath))
	oDlgSysConfig.getControl("DvipngPath").setText(ConvertFromURL(oSystemInfo.DvipngPath))
	oDlgSysConfig.getControl("DvisvgmPath").setText(ConvertFromURL(oSystemInfo.DvisvgmPath))

	' Set the break before num and break after num options
	If oSystemInfo.BreakBeforeNum = "TRUE" Then
		oDlgSysConfig.getControl("BreakBeforeNumCheck").setState(1)
	Else
		oDlgSysConfig.getControl("BreakBeforeNumCheck").setState(0)
	End If
	If oSystemInfo.BreakAfterNum = "TRUE" Then
		oDlgSysConfig.getControl("BreakAfterNumCheck").setState(1)
	Else
		oDlgSysConfig.getControl("BreakAfterNumCheck").setState(0)
	End If

	' Set the equation caption
	oDlgSysConfig.getControl("CaptionTextField").setText(oSystemInfo.EquationCaption)

	' Set the vertical alignment compatibility option
	If oSystemInfo.WordVertAlign = "TRUE" Then
		oDlgSysConfig.getControl("VertAlignCheck").setState(1)
	Else
		oDlgSysConfig.getControl("VertAlignCheck").setState(0)
	End If

	' Set the numbering level
	oDlgSysConfig.getControl("NumLevel").setText(oSystemInfo.NumLevel)

	' Set the shortcuts 
	Dim iKey1 as Integer, iKey2 as Integer, iKey3 as Integer, iKey4 as Integer
	iKey1 = oSystemInfo.EqWriterKey
	If iKey1 <> 0 Then oDlgSysConfig.getControl("Key1").setText(Chr(iKey1-447)) Else oDlgSysConfig.getControl("Key1").setText("")
	iKey2 = oSystemInfo.EqImpressKey
	If iKey2 <> 0 Then oDlgSysConfig.getControl("Key2").setText(Chr(iKey2-447)) Else oDlgSysConfig.getControl("Key2").setText("")
	iKey3 = oSystemInfo.EqDrawKey
	If iKey3 <> 0 Then oDlgSysConfig.getControl("Key3").setText(Chr(iKey3-447)) Else oDlgSysConfig.getControl("Key3").setText("")
	iKey4 = oSystemInfo.NumEqWriterKey
	If iKey4 <> 0 Then oDlgSysConfig.getControl("Key4").setText(Chr(iKey4-447)) Else oDlgSysConfig.getControl("Key4").setText("")
	
	' Check if same Equations shortcuts for all apps
	If iKey1 = iKey2 and iKey1 = iKey3 Then

		oDlgSysConfig.getControl("CheckBoxKey").setState(1)
		SetEquationsShortcutControl(FALSE)

	Else

		oDlgSysConfig.getControl("CheckBoxKey").setState(0)
		SetEquationsShortcutControl(TRUE)

	End If

	' Set the state of the tab buttons
	oDlgSysConfig.getControl("PathsButton1").Model.State=1
	oDlgSysConfig.getControl("OptionsButton1").Model.State=0
	oDlgSysConfig.getControl("ShortcutsButton1").Model.State=0
	oDlgSysConfig.Model.Step = 1
	
	' Show the dialog window
    oDlgSysConfig.Execute()
    
End Sub


' Paths tab button was pressed
Sub PathsButtonPressed(oEvent as Variant)

	' Change the state of the tab buttons
	Dim oDialog as Variant
	oDialog = oEvent.Source.Context.View
	oDialog.getControl("PathsButton1").Model.State=1
	oDialog.getControl("OptionsButton1").Model.State=0
	oDialog.getControl("ShortcutsButton1").Model.State=0
	oDialog.Model.Step = 1

End Sub


' Options tab button was pressed
Sub OptionsButtonPressed(oEvent as Variant)

	' Change the state of the tab buttons
	Dim oDialog as Variant
	oDialog = oEvent.Source.Context.View
	oDialog.getControl("PathsButton2").Model.State=0
	oDialog.getControl("OptionsButton2").Model.State=1
	oDialog.getControl("ShortcutsButton2").Model.State=0
	oDialog.Model.Step = 2

End Sub


' Shortcut tab button was pressed
Sub ShortcutsButtonPressed(oEvent as Variant)

	' Change the state of the tab buttons
	Dim oDialog as Variant
	oDialog = oEvent.Source.Context.View
	oDialog.getControl("PathsButton3").Model.State=0
	oDialog.getControl("OptionsButton3").Model.State=0
	oDialog.getControl("ShortcutsButton3").Model.State=1
	oDialog.Model.Step = 3

	' Refresh items related to the shortcut check box
	If oDlgSysConfig.getControl("CheckBoxKey").getState() Then
		SetEquationsShortcutControl(FALSE)
	Else		
		SetEquationsShortcutControl(TRUE)
	End If

End Sub

' Test the existence of the different external programs in the paths saved in the registry
Sub CheckProgramPaths

	Dim str0 as String, str1 as String, str2 as String, str3 as String, str4 as String
	Dim pgm1 as String, pgm2 as String, pgm3 as String
	Dim prevLatexPath as String, prevDvipngPath as String, prevDviSvgmPath as String

	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)

	' All systems
	str1 = _("Please configure first TexMaths before using it...")
	str2 = _("Can't find the external program ")
	str3 = _("Please check the paths in your TexMaths system configuration...")
	str4 = _("Can't find at least one of the external programs ")
	
	' Windows
	If getGUIType() = 1 Then
	
		pgm1 = "latex.exe"
		pgm2 = "dvipng.exe"
		pgm3 = "dvisvgm.exe"

	
	' Linux or MacOSX
	Else
	
		pgm1 = "latex"
		pgm2 = "dvipng"
		pgm3 = "dvisvgm"
	
	End If

	' Initial values of the external program paths
	prevLatexPath = oSystemInfo.LatexPath
	prevDvipngPath = oSystemInfo.DvipngPath
	prevDvisvgmPath = oSystemInfo.DvisvgmPath
	
	' If program paths are empty, check if programs are in the system path
	If oSystemInfo.LatexPath = "" Then oSystemInfo.LatexPath =	ReadPgmPath( pgm1 )
	If oSystemInfo.DvipngPath = "" Then	oSystemInfo.DvipngPath = ReadPgmPath( pgm2 )
	If oSystemInfo.DvisvgmPath = "" Then oSystemInfo.DvisvgmPath = ReadPgmPath( pgm3 )

	' If the paths changed, save the registry, recreate the script and set default preferences
	If  oSystemInfo.LatexPath <> prevLatexPath Or oSystemInfo.DvipngPath <> prevDvipngPath Or oSystemInfo.DvisvgmPath <> prevDvisvgmPath Then
		oSystemInfo.commitChanges()
		WriteScript(oSystemInfo.LatexPath, oSystemInfo.DvipngPath, oSystemInfo.DvisvgmPath)
		DefaultPrefs()
	End If	

	' Latex program doesn't exist
	If Not FileExists(oSystemInfo.LatexPath & pgm1) Then ProgramNotFound(str2 & pgm1 & chr(10) & str3)
	
	' Read again the registry because it may have changed
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)

	' Both dvipng and dvisvgm programs don't exist
	If Not FileExists(oSystemInfo.DvipngPath & pgm2) And Not FileExists(oSystemInfo.DvisvgmPath & pgm3) Then ProgramNotFound(str4 & pgm2 &  _(" or ") & pgm3 & chr(10) & str3)


	' Script doesn't exist
	If Not FileExists(GetScriptPath()) Then	WriteScript(oSystemInfo.LatexPath, oSystemInfo.DvipngPath, oSystemInfo.DvisvgmPath)

	' Flag to say config was saved
	If oSystemInfo.ConfigSaved = "" Then oSystemInfo.ConfigSaved = "TRUE"
	
	' Save the registry
	oSystemInfo.commitChanges()

End Sub


' Display an appropriate 'Program not found' message
' and launch the config dialog
Sub ProgramNotFound(message as String)

	Msgbox(message)
	SysConfigDialog()

End Sub


' Exit dialog
Sub QuitSysConfig()

	oDlgSysConfig.endExecute()

end sub


Sub SetEquationsShortcutControl( b as Boolean )

		oDlgSysConfig.getControl("AllFrame").Visible = Not b
		oDlgSysConfig.getControl("WriterFrame").Visible = b
		oDlgSysConfig.getControl("ImpressFrame").Visible = b
		oDlgSysConfig.getControl("DrawFrame").Visible = b
		oDlgSysConfig.getControl("Ctrl2Label").Visible = b
		oDlgSysConfig.getControl("Ctrl3Label").Visible = b
		oDlgSysConfig.getControl("Key2").Visible = b
		oDlgSysConfig.getControl("Key3").Visible = b

End Sub


Sub CheckBoxKeyChanged()

	If oDlgSysConfig.getControl("CheckBoxKey").getState() Then
		SetEquationsShortcutControl(FALSE)
	Else		
		SetEquationsShortcutControl(TRUE)
	End If

End Sub


' Save configuration
Sub SaveSysConfig()

	Dim sKey1 as String, sKey2 as String, sKey3 as String, sKey4 as String
	sKey1 = oDlgSysConfig.getControl("Key1").getText()
	
	If oDlgSysConfig.getControl("CheckBoxKey").getState() Then
		sKey2 = sKey1
		sKey3 = sKey1
	Else
		sKey2 = oDlgSysConfig.getControl("Key2").getText()
		sKey3 = oDlgSysConfig.getControl("Key3").getText()
	End If

	If sKey1 = " " or sKey1 = "" Then 
		RemoveShortcut( "EqWriter" )
	ElseIf TestKey(sKey1) <> 0 Then 
		SetShortcut( TestKey(sKey1), sKey1, "EqWriter")
	Else
		Exit Sub
	End If
	
	If sKey2 = " " or sKey2 = "" Then 
		RemoveShortcut( "EqImpress" )
	ElseIf TestKey(sKey2) <> 0 Then 
		SetShortcut( TestKey(sKey2), sKey2, "EqImpress")
	Else
		Exit Sub
	End If

	If sKey3 = " " or sKey3 = "" Then 
		RemoveShortcut( "EqDraw" )
	ElseIf TestKey(sKey3) <> 0 Then 
		SetShortcut( TestKey(sKey3), sKey3, "EqDraw")
	Else
		Exit Sub
	End If

	sKey4 = oDlgSysConfig.getControl("Key4").getText()
	If sKey4 = " " or sKey4 = "" Then 
		RemoveShortcut( "NumEqWriter" )
	ElseIf TestKey(sKey4) <> 0 Then 

		' Check if shortcuts are equal
		If (TestKey(sKey4) = TestKey(sKey1)) Or (TestKey(sKey4) = TestKey(sKey2)) Or (TestKey(sKey4) = TestKey(sKey3)) Then
			msgbox(_("Shortcuts assigned to the Equations and NumberedEquations modules must be different!") & chr(10) &_
			_("Please check your shortcuts...")
			Exit Sub
		End If

		SetShortcut( TestKey(sKey4), sKey4, "NumEqWriter")
	Else
		Exit Sub
	End If

	' Create the TexMaths script
	' Get the variables from the sysconfig dialog
	Dim sLatexPath as String, sDvipngPath as String, sDvisvgmPath as String
	Dim oFileAccess as Variant

	sLatexPath = ConvertToURL(oDlgSysConfig.getControl("LatexPath").getText())
	sDvipngPath = ConvertToURL(oDlgSysConfig.getControl("DvipngPath").getText())
	sDvisvgmPath = ConvertToURL(oDlgSysConfig.getControl("DvisvgmPath").getText())
	
	If sLatexPath = "" Then
		
		Msgbox _("Please set the latex path...")
		Exit Sub
		
	End If
	
	If sDvipngPath = "" and sDvisvgmPath = "" Then
		
		Msgbox _("Please set at least one dvipng path or the dvisvgm path...")
		Exit Sub
		
	End If
	
	' Add a / at the end if necessary
	sLatexPath = CheckPath(sLatexPath)
	If sDvipngPath <>  "" Then sDvipngPath = CheckPath(sDvipngPath)
	If sDvisvgmPath <>  "" Then sDvisvgmPath = CheckPath(sDvisvgmPath)

	' Windows
	If getGUIType() = 1 Then 	

		If CheckFile( sLatexPath & "latex.exe", _("The program latex.exe is not located in ") & ConvertFromURL(sLatexPath) ) Then Exit Sub

		If sDvipngPath <>  "" Then
			 If CheckFile( sDvipngPath & "dvipng.exe", _("The program dvipng.exe is not located in ") & ConvertFromURL(sDvipngPath) ) Then Exit Sub
		End If

		If sDvisvgmPath <>  "" Then
			If CheckFile( sDvisvgmPath & "dvisvgm.exe", _("The program dvisvgm.exe is not located in ") & ConvertFromURL(sDvisvgmPath) ) Then Exit Sub
		End If

	' Linux or MacOSX
	Else
	
		If CheckFile( sLatexPath & "latex", _("The program latex is not located in ") & ConvertFromURL(sLatexPath) ) Then Exit Sub

		If sDvipngPath <>  "" Then
			If CheckFile(sDvipngPath & "dvipng", _("The program dvipng is not located in ") & ConvertFromURL(sDvipngPath) ) Then Exit Sub
		End If

		If sDvisvgmPath <>  "" Then
			If CheckFile( sDvisvgmPath & "dvisvgm",	_("The program dvisvgm is not located in ") & ConvertFromURL(sDvisvgmPath) ) Then Exit Sub
		End If

	End If

	' Open service file and an output stream
	Dim cURL as String
	oFileAccess = createUnoService("com.sun.star.ucb.SimpleFileAccess")		
	cURL = ConvertToURL( GetScriptPath() )

	' Get the path saved in the registry
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)	
	
	If sLatexPath <> oSystemInfo.LatexPath _
	or sDvipngPath <> oSystemInfo.DvipngPath _
	or sDvisvgmPath <> oSystemInfo.DvisvgmPath _
	or Not oFileAccess.exists(cURL) Then
		WriteScript(sLatexPath, sDvipngPath, sDvisvgmPath)	
	End If

	oDlgSysConfig.endExecute()

	' Set vertical alignment compatibility option
	If oDlgSysConfig.getControl("VertAlignCheck").getState() Then oSystemInfo.WordVertAlign = "TRUE" Else oSystemInfo.WordVertAlign = "FALSE"

	' Set break before num and break after num options
	If oDlgSysConfig.getControl("BreakBeforeNumCheck").getState() Then oSystemInfo.BreakBeforeNum = "TRUE" Else	oSystemInfo.BreakBeforeNum = "FALSE"
	If oDlgSysConfig.getControl("BreakAfterNumCheck").getState() Then oSystemInfo.BreakAfterNum = "TRUE" Else oSystemInfo.BreakAfterNum = "FALSE"

	' Set equation caption
	oSystemInfo.EquationCaption = oDlgSysConfig.getControl("CaptionTextField").getText()	

	' Set numbering level
	oSystemInfo.NumLevel = oDlgSysConfig.getControl("NumLevel").getText()	

	' Config was saved
	oSystemInfo.ConfigSaved = "TRUE"
	oSystemInfo.commitChanges()

	' Set configuration in case we were called by TexMathsEquations
	TexMathsEquations.SetConfig()

End Sub


' Test shortcut validity
Function TestKey(sKey as String) as Integer

	If sKey = "" Then
	
		TestKey() = 0
	
	Else
	
		Dim i as Integer
		i = Asc(sKey)
	
		If 96 < i and i < 123 Then 
			TestKey() = i + 415 
		ElseIf 64 < i and i < 91  Then 
			TestKey() = i + 447
		ElseIf sKey <> " " Then
			Msgbox( _("Only characters from A to Z are allowed for shortcuts!") )
			TestKey() = 0 
		End If
		
	End If

End Function


' Add shortcut to the registry
Sub SetShortcut(iKey as Integer, sKey as String, sRegId)

	Dim sNewCommand as String, sCommand as String, sModule as String, sDoc as String
	
	' Retrieve the module configuration manager from central module configuration manager supplier
	Dim oModuleCfgMgrSupplier as Variant 
	oModuleCfgMgrSupplier = createUnoService("com.sun.star.ui.ModuleUIConfigurationManagerSupplier")

	' Retrieve the module configuration manager based on the registry identifier
	Dim oModuleCfgMgr as Variant
	
	If sRegId = "EqWriter" or sRegId = "NumEqWriter" Then

		' Writer installed
		If ComponentInstalled( "Writer" ) Then
			sDoc = "Writer"
			oModuleCfgMgr = oModuleCfgMgrSupplier.getUIConfigurationManager("com.sun.star.text.TextDocument")
		
		' Writer not installed
		Else
 			Exit Sub
		End If

	
	ElseIf sRegId = "EqImpress" Then
    
		' Impress installed
		If ComponentInstalled( "Impress" ) Then
			sDoc = "Impress"
			oModuleCfgMgr = oModuleCfgMgrSupplier.getUIConfigurationManager("com.sun.star.presentation.PresentationDocument")
		
		' Impress not installed
		Else
 			Exit Sub
		End If


	ElseIf sRegId = "EqDraw" Then

		' Draw installation was already checked in Main()
		sDoc = "Draw"
		oModuleCfgMgr = oModuleCfgMgrSupplier.getUIConfigurationManager("com.sun.star.drawing.DrawingDocument")
	
	End If
	
	Dim oShortcutMgr as Variant
	oShortcutMgr = oModuleCfgMgr.getShortcutManager
	
	Dim aKeyEvent as New com.sun.star.awt.KeyEvent
	With aKeyEvent
		.Modifiers = com.sun.star.awt.KeyModifier.MOD1
		.KeyCode = iKey  ' com.sun.star.awt.Key.E
	End With
		
	' Get the actual command for the shortcut "iKey"
	On Error Resume Next
	sCommand = oShortcutMgr.getCommandByKeyEvent(aKeyEvent)
	On Error GoTo 0 ' Restore the error handler

	' Set the TexMaths module name
	If sRegId = "NumEqWriter" Then
		sModule = "NumberedEquations"
	Else
		sModule = "Equations"
	End If	

	' Set the new command for the appropriate TexMaths module (Equations or NumberedEquations)
	sNewCommand = "vnd.sun.star.script:TexMaths.TexMaths" & sModule & ".main?language=Basic&location=application"

	' Get the shortcut repository
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo" , TRUE)	

	If len(sCommand) = 0 Then ' New command
	
		' Restore the previous shortcut command
		If (sRegId = "EqWriter" And oSystemInfo.EqWriterKey > 0)_
		Or (sRegId = "NumEqWriter" And oSystemInfo.NumEqWriterKey > 0)_
		Or (sRegId = "EqImpress" And oSystemInfo.EqImpressKey > 0)_
		Or (sRegId = "EqDraw" And oSystemInfo.EqDrawKey > 0)  Then
			RemoveShortcut( sRegId )
		End If				

		oShortcutMgr.setKeyEvent( aKeyEvent, sNewCommand )
		oShortcutMgr.store
		
		' Store the current command
		If (sRegId = "EqWriter") Then
			oSystemInfo.EqWriterCmd = ""
		ElseIf (sRegId = "NumEqWriter") Then
			oSystemInfo.NumEqWriterCmd = ""
		ElseIf (sRegId = "EqImpress") Then
			oSystemInfo.EqImpressCmd = ""
		ElseIf (sRegId = "EqDraw") Then
			oSystemInfo.EqDrawCmd = ""
		End If				

	ElseIf sCommand <> sNewCommand Then ' The key event is already used by another command

		Dim sMsg as String
		sMsg = sDoc & Chr(10) & Chr(10)
		sMsg = sMsg & _("The key combination CTRL + ") & sKey &  _(" is already assigned to the command ") & "'" & sCommand & "'" & Chr(10) & Chr(10)
		sMsg = sMsg & _("Do you want to set the command") & " TexMaths" & sModule & ".main ?"
					   
		Dim iMsgResult as Variant
		iMsgResult = MsgBox( sMsg, 1)
		If iMsgResult <> 1 Then Exit Sub

		' Remove the previous shortcut command
		If (sRegId = "EqWriter" And oSystemInfo.EqWriterKey > 0)_
		Or (sRegId = "NumEqWriter" And oSystemInfo.NumEqWriterKey > 0)_
		Or (sRegId = "EqImpress" And oSystemInfo.EqImpressKey > 0)_
		Or (sRegId = "EqDraw" And oSystemInfo.EqDrawKey > 0)  Then
			RemoveShortcut( sRegId )
		End If

		oShortcutMgr.removeKeyEvent(aKeyEvent)
		oShortcutMgr.setKeyEvent(aKeyEvent, sNewCommand )
		oShortcutMgr.store
		
		' Store the current command
		If sRegId = "EqWriter" Then
			oSystemInfo.EqWriterCmd = sCommand
		ElseIf sRegId = "NumEqWriter" Then
			oSystemInfo.NumEqWriterCmd = sCommand
		ElseIf sRegId = "EqImpress" Then
			oSystemInfo.EqImpressCmd = sCommand
		ElseIf sRegId = "EqDraw" Then
			oSystemInfo.EqDrawCmd = sCommand
		End If				

	Else ' Same command

		' Commmand previously set outside TexMaths => put it at spaces
		If (sRegId = "EqWriter" And oSystemInfo.EqWriterCmd = sCommand)_
		Or (sRegId = "NumEqWriter" And oSystemInfo.NumEqWriterCmd = sCommand)_
		Or (sRegId = "EqImpress" And oSystemInfo.EqImpressCmd = sCommand)_
		Or (sRegId = "EqDraw" And oSystemInfo.EqDrawCmd = sCommand) Then
			sCommand = ""	
		End If				

	End if

	' Save to the registry the new shortcut with the previous command
	If sRegId = "EqWriter" Then
		oSystemInfo.EqWriterKey = iKey
	ElseIf sRegId = "NumEqWriter" Then
		oSystemInfo.NumEqWriterKey = iKey
	ElseIf sRegId = "EqImpress" Then
		oSystemInfo.EqImpressKey = iKey
	ElseIf sRegId = "EqDraw" Then
		oSystemInfo.EqDrawKey = iKey
	End If				
	oSystemInfo.commitChanges()

End Sub


' Remove shortcut from the registry
Sub RemoveShortcut(sRegId as String)

	Dim iKey as Integer
	Dim sCommand as String, sPrevCommand as String, sDoc as String

	' Get the current shortcut and previous command from the registry
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo/", TRUE)

	If sRegId = "EqWriter" Then

		iKey = oSystemInfo.EqWriterKey
		sPrevCommand = oSystemInfo.EqWriterCmd
		oSystemInfo.EqWriterKey = ""
		oSystemInfo.EqWriterCmd = ""
		sDoc = "Writer"

	ElseIf sRegId = "NumEqWriter" Then

		iKey = oSystemInfo.NumEqWriterKey
		sPrevCommand = oSystemInfo.NumEqWriterCmd
		oSystemInfo.NumEqWriterKey = ""
		oSystemInfo.NumEqWriterCmd = ""
		sDoc = "Writer"

	ElseIf sRegId = "EqImpress" Then

		iKey = oSystemInfo.EqImpressKey
		sPrevCommand = oSystemInfo.EqImpressCmd
		oSystemInfo.EqImpressKey = ""
		oSystemInfo.EqImpressCmd = ""
		sDoc = "Impress"

	ElseIf sRegId = "EqDraw" Then

		iKey = oSystemInfo.EqDrawKey
		sPrevCommand = oSystemInfo.EqDrawCmd
		oSystemInfo.EqDrawKey = ""
		oSystemInfo.EqDrawCmd = ""
		sDoc = "Draw"

	End If				

	' If no shortcut defined in the registry => exit sub
	If iKey = 0 or iKey = "" Then Exit Sub
	
	' Retrieve the module configuration manager from central module configuration manager supplier
	Dim oModuleCfgMgrSupplier as Variant
	oModuleCfgMgrSupplier = createUnoService("com.sun.star.ui.ModuleUIConfigurationManagerSupplier")

	' Retrieve the module configuration manager with module identifier
	Dim oModuleCfgMgr as Variant

	If sDoc = "Writer" Then

		' Writer installed
		If ComponentInstalled( "Writer" ) Then
			oModuleCfgMgr = oModuleCfgMgrSupplier.getUIConfigurationManager("com.sun.star.text.TextDocument")
		
		' Writer not installed
		Else
 			Exit Sub
		End If

	ElseIf sDoc = "Impress" Then

		' Impress installed
		If ComponentInstalled( "Impress" ) Then
			oModuleCfgMgr = oModuleCfgMgrSupplier.getUIConfigurationManager("com.sun.star.presentation.PresentationDocument")
		
		' Impress not installed
		Else
 			Exit Sub
		End If

	ElseIf sDoc = "Draw" Then 
		
		' Draw installation was already checked in Main()
		oModuleCfgMgr = oModuleCfgMgrSupplier.getUIConfigurationManager("com.sun.star.drawing.DrawingDocument")
	
	End If
	
	Dim oShortcutMgr as Variant
	oShortcutMgr = oModuleCfgMgr.getShortcutManager
		
	Dim aKeyEvent as New com.sun.star.awt.KeyEvent
	With aKeyEvent
		.Modifiers = com.sun.star.awt.KeyModifier.MOD1
		.KeyCode = iKey
	End With
	
	On Error Resume Next
	sCommand = oShortcutMgr.getCommandByKeyEvent(aKeyEvent)
	On Error GoTo 0 ' Restore the error handler

	' Remove the current shortcut
	If len(sCommand) <> 0 Then
		oShortcutMgr.removeKeyEvent(aKeyEvent)
		oShortcutMgr.store
	End If
	
	' Set the previous shortcut
	If len(sPrevCommand) <> 0 Then 
		oShortcutMgr.setKeyEvent( aKeyEvent, sPrevCommand )
		oShortcutMgr.store
	End If

	' Save change in the registry
	oSystemInfo.commitChanges()	

End Sub


' Create the TexMaths script depending on the system
' Note : on Windows, the Shell() command is buggy and does not work correctly
' if the script path contains spaces. As a workaround, we put the script in a
' directory named C:\texmaths-<username_without_spaces> and we set it as a
' hidden directory
Sub WriteScript(sLatexPath as String, sDvipngPath as String, sDvisvgmPath as String)

	Dim cURL as String, sPaths as String, sTexMaths as String
	Dim sDvipngCmd as String, sDvisvgmCmd as String

	' Save the paths to the registry
	Dim oSystemInfo as Variant
	oSystemInfo = GetConfigAccess( "/ooo.ext.texmaths.Registry/SystemInfo", TRUE)
	oSystemInfo.LatexPath = sLatexPath
	oSystemInfo.DvipngPath = sDvipngPath
	oSystemInfo.DvisvgmPath = sDvisvgmPath
	oSystemInfo.commitChanges()

	' Open service file and an output stream
	Dim oFileAccess as Variant, oTextStream as Variant
	oFileAccess = createUnoService("com.sun.star.ucb.SimpleFileAccess")
	oTextStream  = createUnoService("com.sun.star.io.TextOutputStream")

	' Generate the TexMaths script in the appropriate directory
	cURL = ConvertToURL( GetScriptPath() )
	If oFileAccess.exists( cURL ) Then oFileAccess.kill( cURL )

	' => Windows: generate a .bat script
	If getGUIType() = 1 Then

		' Paths
		sPaths = "set PATH=" & ConvertFromURL(sLatexPath) & ";" & ConvertFromURL(sDvipngPath) & ";" & ConvertFromURL(sDvisvgmPath) & ";%PATH%" & chr(10)
	
		' Script
		If sDvipngPath = "" Then

			sDvipngCmd = ""

		Else

			sDvipngCmd = "rem Conversion of the DVI file to a PNG image" & chr(10) &_
			"if ""%EXT%"" == ""png"" (" & chr(10) &_
			"if ""%TRANS%"" == ""TRUE"" (" & chr(10) &_
			"   dvipng -q -T tight -bg Transparent --width --height --depth -D %DPI% -o %FILENAME%.png %FILENAME%.dvi > %FILENAME%.dat" & chr(10) &_
			") else (" & chr(10) &_
			"   dvipng -q -T tight --width --height --depth -D %DPI% -o %FILENAME%.png %FILENAME%.dvi > %FILENAME%.dat" & chr(10) &_
			")" & chr(10) &_
			")" & chr(10)

		End If
		
		If sDvisvgmPath = "" Then

			sDvisvgmCmd = ""

		Else

			sDvisvgmCmd = "rem Conversion of the DVI file to a SVG image" & chr(10) &_
			"if ""%EXT%"" == ""svg"" (" & chr(10) &_
			"   dvisvgm -n1 %FILENAME%.dvi 2> %FILENAME%.dat" & chr(10) &_
			")" & chr(10)
	
		End If

		sTexMaths = "@echo off" & chr(10) &_
		chr(10) &_
		"rem This script is part of the TexMaths package" & chr(10) &_
		"rem http://roland65.free.fr/texmaths" & chr(10) &_
		"rem" & chr(10) &_
		"rem Roland Baudin (roland65@free.fr)" & chr(10) &_
		chr(10) &_
		"rem Process the options" & chr(10) &_
		"set EXT=%1" & chr(10) &_
		"set DPI=%2" & chr(10) &_
		"set TRANS=%3" & chr(10) &_
		"set TMPPATH=%4" & chr(10) &_
		"set FILENAME=tmpfile" & chr(10) &_
		chr(10) &_
		"rem Set Paths" & chr(10) &_
		sPaths & chr(10) &_
		"rem Generate system log" & chr(10) &_
		"call :SystemLog" & chr(10) &_
		chr(10) &_
		"rem Go to the tmp directory" & chr(10) &_
		"if not %TMPPATH% == """" (" & chr(10) &_
		"   if not exist %TMPPATH% (" & chr(10) &_
		"      mkdir %TMPPATH%" & chr(10) &_
		"   )" & chr(10) &_
		"	cd /D %TMPPATH%" & chr(10) &_
		")" & chr(10) &_
		chr(10) &_
		"rem Remove old files but not the tex file" & chr(10) &_
		"del /Q *.aux" & chr(10) &_
		"del /Q *.bsl" & chr(10) &_
		"del /Q *.dvi" & chr(10) &_
		"del /Q *.log" & chr(10) &_
		"del /Q *.out" & chr(10) &_
		"del /Q *.dat" & chr(10) &_
		"del /Q *.png" & chr(10) &_
		"del /Q *.svg" & chr(10) &_
		chr(10) &_
		"rem Creation of the DVI file" & chr(10) &_
		"latex -interaction=nonstopmode %FILENAME%.tex  > %FILENAME%.out" & chr(10) &_
		chr(10) &_
		sDvisvgmCmd & chr(10) &_
		sDvipngCmd & chr(10) &_
		chr(10) &_
		"Goto :EOF"& chr(10) &_
		chr(10) &_
		"rem Function used to generate system log" & chr(10) &_
		":SystemLog" & chr(10) &_
		chr(10) &_
		"set SYSLOG=%TMPPATH%\..\System.log" & chr(10) &_
		"rem del %SYSLOG%" & chr(10) &_
		"for /f ""tokens=*"" %%a in ('VER') do set VERSION=%%a" & chr(10) &_
		"echo osType= %VERSION% > %SYSLOG%" & chr(10) &_
		"echo. >> %SYSLOG%" & chr(10) &_
		"echo PATH= %PATH% >> %SYSLOG%" & chr(10) &_
		chr(10) &_
		"Goto :EOF"

	' => Linux or MacOSX: generate a shell script
	Else
		
		' Paths
		sPaths = "cpu=$(uname -p)" & chr(10) &_
		"mac=$(uname -m)" & chr(10) &_
		"sys=$(uname -s)" & chr(10) &_
		"[ ""$cpu"" = ""powerpc"" -o ""$mac"" = ""ppc"" ] && cpuType=ppc || cpuType=i386 " & chr(10) &_
		"[ ""$sys"" = ""Darwin"" ] && osType=MacOSX || osType=Linux " & chr(10) &_
		"UserDir=""" & ConvertFromURL(glb_sUserPath) & """" & chr(10) &_
		"PkgDir=""" & ConvertFromURL(glb_sPkgPath) & """" & chr(10) &_
		chr(10) &_
		"PATH=""${PkgDir}bin/${osType}${cpuType}:$PATH""" & chr(10) &_
		"PATH=""" & ConvertFromURL(sLatexPath) & ":" & ConvertFromURL(sDvipngPath) & ":" & ConvertFromURL(sDvisvgmPath) & ":$PATH""" & chr(10) &_
		"export PATH" & chr(10) &_
		chr(10) &_
		"if [ ""$osType"" = ""MacOSX"" ]" & chr(10) &_
		"then" & chr(10) &_
		"   DYLD_LIBRARY_PATH=""${PkgDir}lib/${cpuType}/""" & chr(10) &_
		"   export DYLD_LIBRARY_PATH" & chr(10) &_
		"else" & chr(10) &_
		"   LD_LIBRARY_PATH=""${PkgDir}lib/${cpuType}/""" & chr(10) &_
		"   export LD_LIBRARY_PATH" & chr(10) &_
		"fi" & chr(10)
		
		' Script
		If sDvipngPath = "" Then
			sDvipngCmd = ""
		Else
			sDvipngCmd = "# Conversion of the DVI file to a PNG image" & chr(10) &_
			"if [ -f ${filename}.dvi ]" & chr(10) &_
			"then" & chr(10) &_
			"if [ ""${ext}"" = ""png"" ]" & chr(10) &_
			"then" & chr(10) &_
			"if [ ""${Trans}"" = ""TRUE"" ]" & chr(10) &_
			"then" & chr(10) &_
			"   dvipng -q -T tight -bg Transparent --width --height --depth -D ${dpi} -o ${filename}.png ${filename}.dvi > ${filename}.dat" & chr(10) &_
			"else" & chr(10) &_
			"   dvipng -q -T tight --width --height --depth -D ${dpi} -o ${filename}.png ${filename}.dvi > ${filename}.dat" & chr(10) &_
			"fi" & chr(10) &_
			"fi" & chr(10) &_
			"fi"

		End If
		
		If sDvisvgmPath = "" Then
			sDvisvgmCmd = ""
		Else
			sDvisvgmCmd = "# Conversion of the DVI file to a SVG image" & chr(10) &_
			"if [ -f ${filename}.dvi ]" & chr(10) &_
			"then" & chr(10) &_
			"if [ ""${ext}"" = ""svg"" ]" & chr(10) &_
			"then" & chr(10) &_
			"   dvisvgm -n1 ${filename}.dvi &> ${filename}.dat" & chr(10) &_
			"fi" & chr(10) &_
			"fi"
		End If

		sTexMaths = "#!/bin/bash" & chr(10) &_
		"#" & chr(10) &_
		"# This script is part of the TexMaths package" & chr(10) &_
		"# http://roland65.free.fr/texmaths" & chr(10) &_
		"#" & chr(10) &_
		"# Roland Baudin (roland65@free.fr)" & chr(10) &_
		chr(10) &_
		"# Function used to generate system log" & chr(10) &_
		"SystemLog(){" & chr(10) &_
		"syslog=${UserDir}System.log" & chr(10) &_
		"uname -a > $syslog" & chr(10) &_
		"echo """" >> $syslog" & chr(10) &_
		"echo ""osType=$osType"" >> $syslog" & chr(10) &_
		"echo ""cpuType=$cpuType"" >> $syslog" & chr(10) &_
		"echo """" >> $syslog" & chr(10) &_
		"echo ""PATH=$PATH"" >> $syslog" & chr(10) &_
		"echo """" >> $syslog" & chr(10) &_
		"echo ""LD_LIBRARY_PATH=$LD_LIBRARY_PATH"" >> $syslog" & chr(10) &_
		"echo ""DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH"" >> $syslog" & chr(10) &_
		"}" & chr(10) &_
		chr(10) &_
		"# Definition of PATH and binary paths" & chr(10) &_
		sPaths & chr(10) &_
		"# Process the options" & chr(10) &_
		"ext=$1" & chr(10) &_
		"dpi=$2" & chr(10) &_
		"Trans=""$3""" & chr(10) &_
		"tmpPath=""$4""" & chr(10) &_
		"filename=tmpfile" & chr(10) &_
		chr(10) &_
		"# Generate system log" & chr(10) &_
		"SystemLog" & chr(10) &_
		chr(10) &_
		"# Go to the tmp directory" & chr(10) &_
		"if [ ""$tmpPath"" != """" ] " & chr(10) &_
		"then" & chr(10) &_
		"   [ ! -d ""$tmpPath"" ]  && mkdir -p ""$tmpPath""" & chr(10) &_
		"   cd ""$tmpPath""" & chr(10) &_
		"fi" & chr(10) &_
		chr(10) &_
		"# Remove old files but not the tex file" & chr(10) &_
		"rm ${filename}.{dvi,out,bsl,dat,log,aux,png,svg} &> /dev/null" & chr(10) &_
		chr(10) &_
		"# Creation of the DVI file" & chr(10) &_
		"latex -interaction=nonstopmode ${filename}.tex  > ${filename}.out" & chr(10) &_
		chr(10) &_
		sDvisvgmCmd & chr(10) &_
		sDvipngCmd & chr(10)

 	End if
		
	oTextStream.setOutputStream(oFileAccess.openFileWrite(cURL))
	oTextStream.writeString( sTexMaths )
	oTextStream.closeOutput()
	
	' On Windows, hide the script file because it is located in C:\ and the user should not see it
	If getGUIType() = 1 Then

		SetAttr( GetScriptDir() ,2 )  ' 2 => hidden attribute

	End If

	
End Sub

