#!/bin/bash
set -e
set -o nounset
set -x 

if [ "$1" == "" ]
then
    echo "Usage: $0 TexMaths-version.oxt"
    echo "Always extracts into the extension directory, overwriting any files there."
    exit -1
fi

unzip -o "$1" -d extension/

cat > extension/TexMaths/module.dtd <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!ELEMENT script:module (#PCDATA)>
<!ATTLIST script:module
	xmlns:script CDATA #FIXED "http://openoffice.org/2000/script"
	script:name CDATA #REQUIRED
	script:language CDATA #REQUIRED
>
EOF

for xba in extension/TexMaths/*.xba
do
    xmlstarlet sel -N x="http://openoffice.org/2000/script" -t -v '//x:module' $xba | recode HTML_4.0 | sed 's/Â§/§/g' > src/`basename $xba .xba`.bas
    rm $xba
done
rm extension/TexMaths/module.dtd
rm extension/po/xgettextbas
rm extension/makepkg
